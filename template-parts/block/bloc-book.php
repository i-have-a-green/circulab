<?php
/**
 * Block Name: Bloc Book
 *
 */
 ?>
 <div id="bookModal" class="modal">
	<button class="closeModal">X</button>
    <iframe src="<?php the_field("pdf");?>" width="100%" height="100%" border="0"></iframe>
</div>
<div class="bloc-book-container bloc designColorBlue">
    <div class="bookText blocPadding">
      <div class="picto bg-sky-blue">
          <?php echo file_get_contents( get_attached_file( get_field("picto")) ); ?>
      </div>
      <h2 class="fontTitle"><?php the_field("title");?></h2>
      <div class="separator bg-sky-blue"></div>
      <?php the_field("content");?>
      <?php
          if($buttons = get_field("boutons")):
              foreach ($buttons as $key => $button) : $id_modal = "modal-".uniqid(); ?>
                  <a href="#" class="button buttonCustomColor bookButton buttonModal" data-id-modal="<?php echo $id_modal;?>" style="background-color:<?php echo $button["bg-color"];?>;"><?php echo $button["label"];?></a>
                  <div id="<?php echo $id_modal;?>" class="modal">
                        <button class="closeModal">X</button>
                        <iframe src="<?php echo $button["link"];?>" width="100%" height="100%" border="0"></iframe>
                    </div>
              <?php endforeach;
          endif;
      ?>
    </div>
    <div class="bookCover">
      <?php
          $image = get_field("image");
          $size = 'large'; 
          if( $image ) {
              echo wp_get_attachment_image($image, $size );
          }
      ?>
        <?php if(get_field("pdf") && !empty(get_field("pdf"))):?>
            <a href="" class="button buttonModal buttonGreen bookCta" data-id-modal="bookModal"><?php _e("Read", "circulab");?></a>
        <?php endif;?>
    </div>
</div>
