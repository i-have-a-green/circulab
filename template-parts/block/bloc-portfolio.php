<?php
/**
 * Block Name: Bloc Portfolio
 *
 */
 ?>
 <div class="bloc-portfolio ">
    <?php
    $images = get_field('images');
    if ( $images ) :
        foreach ($images as $key => $portfolio) :

            ?>
            <div class="bloc portfolio">
                <div class="content-portfolio">
                    <a href="<?php echo $portfolio["link"];?>" >
                        <?php echo $portfolio["content"];?>
                    </a>
                </div>
                <?php
                $image = $portfolio["image"];
                $size = 'portfolio';
                if( $image ) {
                    echo wp_get_attachment_image($image, $size );
                }
                ?>
            </div>
            <?php
        endforeach;
    endif;
    ?>
</div>
