<?php
/**
 * Block Name: Bloc Portfolio Full Width
 *
 */
 ?>
 <div class="bloc-portfolio bloc-portfolio-full-width">
    <?php
    $images = get_field('images');
    if ( $images ) :
        foreach ($images as $key => $portfolio) :

            ?>
            <div class="bloc portfolio portfolioFullWidth">
                <div class="content-portfolio">
                    <a href="<?php echo $portfolio["link"];?>" >
                        <?php echo $portfolio["content"];?>
                    </a>
                </div>
                <?php
                $image = $portfolio["image"];
                $size = 'portfolioFull';
                if( $image ) {
                    echo wp_get_attachment_image($image, $size );
                }
                ?>
            </div>
            <?php
        endforeach;
    endif;
    ?>
</div>
