<?php
/**
 * Block Name: Bloc Paragraphe
 *
 */
 ?>
<!-- Same layout as bloc-defis -->

<div class="bloc-paragraphe-container bloc-defis bloc ">

    <div class="defisText">
          <h2 class="titleSection fontTitle"><?php the_field("title");?></h2>
          <?php the_field("content");?>
    </div><!--.defisText -->

    <div class="defisImage">
      <?php
          $image = get_field("image");
          $size = 'full';
          if( $image ) {
              echo wp_get_attachment_image($image, $size );
          }
      ?>
    </div>

</div>
