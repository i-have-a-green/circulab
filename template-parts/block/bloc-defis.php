<?php
/**
 * Block Name: Bloc Défis
 */
 ?>

<div class="bloc-defis bloc">
  <div class="defisText">
    <h2 class="titleSection fontTitle"><?php the_field("title");?></h2>
    <?php the_field("content");?>
    <?php if(!empty(get_field("label")) && !empty(get_field("link"))):?>
        <a href="<?php echo get_field("link");?>" class="button buttonPink"><?php echo get_field("label");?></a>
    <?php endif;?>
    <?php if(!empty(get_field("label2")) && !empty(get_field("link2"))):?>
        <a href="<?php echo get_field("link2");?>" class="button buttonPink"><?php echo get_field("label2");?></a>
    <?php endif;?>
  </div>
  <div class="defisImage">
    <?php
        $image = get_field('image');
        $size = 'defis'; //possibilité d'ajouter une taille dans images.php
        if( $image ) {
            echo wp_get_attachment_image( $image, $size );
        }
    ?>
  </div>
</div>
