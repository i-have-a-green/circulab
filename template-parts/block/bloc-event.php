<?php
/**
 * Block Name: Bloc event
 */
 ?>
<div class="bloc-event-container">
    <h2 class="titleSection fontTitle"><?php _e("Events", "circulab");?></h2>

    <div class="eventCardContainer"><!-- eventCardContainer -->
    <?php
    $args = array(
        'posts_per_page' => 4,
        'post_type'   => "event",
        'post_status' => 'publish',
        'meta_query' => array(
            array(
            'key'	=> 'date_event',
            'compare'	=> '>=',
            'value'	=> date('Ymd'),
            'type'	=> 'DATETIME'
            ),
        ),
        'order'	=> 'ASC',
        'orderby'	=> 'meta_value',
        'meta_key'	=> 'date_event',
        'meta_type'	=> 'DATETIME',
    );
    query_posts($args);
    ?>


    <?php
    if ( have_posts() ) :
        while (have_posts()) : the_post();
            get_template_part('template-parts/archive', 'event');
        endwhile;
    ?>
    <?php
    else :
        _e("Aucun contenu", "arpdl");
        $no_results = true;
    endif;
    wp_reset_query();
    ?>

  </div><!-- eventCardsContainer  -->
    <a href="<?php the_permalink( get_field("archive_event_".weglot_get_current_language(), "option") )?>" class="button buttonBlue"><?php _e("All the events", "circulab");?></a>
</div>
