<?php
/**
 * Block Name: Bloc Title
 *
 */
 ?>

<!-- Slideshow container -->

<div class="slideshow-container">

	<!-- quotes -->
	<?php
	// Check rows exists.
	if( have_rows('quotes') ):
		// Loop through rows.
		while( have_rows('quotes') ) : the_row();?>
			<?php 
			$quote = get_sub_field('quote');
			?>
			<div class="mySlides fade" >
				<?php echo $quote;?>
			</div>                   
			
		<?php // End loop.
		endwhile;

	endif;?>

	<!-- Previous & next buttons -->
	<button class="prev" onclick="plusSlides(-1)"><svg viewbox="0 0 166 166" width="30px" height="30px" style="fill:white"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"></path></svg></button>
	<button class="next" onclick="plusSlides(1)"><svg viewbox="0 0 166 166" width="30px" height="30px" style="fill:white"><path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg></button>
</div>

       




