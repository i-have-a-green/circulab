<?php
/**
 * Block Name: Bloc Circulab
 */
 ?>
<div class="bloc-circulab-container  <?php the_field("display");?>">
    <?php
    $subBloc = get_field('subBloc');
    if ( !$subBloc ) :
    ?>
        <em>Renseigner les blocs ...</em>
    <?php
    else :
        foreach ($subBloc as $key => $bloc) :
            $bg = false;
            $pano = false;
            if($bloc["imagePosition"] == "background"){
                $image = $bloc["image"];
                $size = 'bg';
                if( $image ) {
                    $bgUrl = wp_get_attachment_image_url($image, $size );
                    $bg = true;
                }
            }
            else{
                $image = $bloc["image"];
                $size = 'bg';
                if( $image ) {
                    $pano = true;
                    $imagePano = wp_get_attachment_image($image, $size );
                }
            }
            ?>
            <div
                class="bloc blocCirculab
                    textColor-<?php echo $bloc["txtColor"]; ?>
                    bg-<?php echo $bloc["bg"]; ?>
                    <?php echo ($bg) ? "background-image" : "";?>
                    <?php echo ($pano) ? "hasPano" : "";?>
                    <?php if(!empty($bloc["buttonLabel"]) && !empty($bloc["buttonLink"])) echo "hasCta"?>
                    designColor-<?php echo $bloc["designColor"]; ?> "
                <?php if ($bg):?>
                    style="background-image:url(<?php echo $bgUrl;?> );"
                <?php endif;?> >

                <div class="blocCirculabContent
                  <?php if(!empty($bloc["buttonLabel"]) && !empty($bloc["buttonLink"])) echo "ctaMarginTrick"?> ">
                  <?php if(!empty($bloc["picto"])): //le acf oblige le format svg?>
                      <div class="picto pictoPosition-<?php echo $bloc["pictoPosition"];?>">
                          <?php echo file_get_contents( get_attached_file( $bloc["picto"]) ); ?>
                      </div>
                  <?php endif;?>

                  <?php if(!empty($bloc["title"])): ?>
                    <h2 class="title fontTitle marginPictoPosition-<?php echo $bloc["pictoPosition"];?>"></span><?php echo $bloc["title"]; ?></h2>
                    <div class="separator"></div>
                  <?php endif;?>

                  <?php echo $bloc["content"]; ?>
                </div>
                <?php echo ($pano)?$imagePano:"";?>

                <?php if(!empty($bloc["buttonLabel"]) && !empty($bloc["buttonLink"])):?>
                    <?php if($bloc["open_modal"]): $idModal = uniqid();?>
                        <a href="#" class="button ctaCirculab buttonModal" data-id-modal="modal-<?php echo $idModal;?>"><?php echo $bloc["buttonLabel"];?></a>
                        <div class="modal" id="modal-<?php echo $idModal;?>">
                            <button class="closeModal buttonGhost">X</button>
                            <iframe src="<?php echo $bloc["buttonLink"];?>" width="100%" height="100%" frameBorder="0"></iframe>
                        </div>
                    <?php else:?>
                        <a href="<?php echo $bloc["buttonLink"];?>" class="button ctaCirculab"><?php echo $bloc["buttonLabel"];?></a>
                  <?php endif;?>
                <?php endif;?>

            </div>
            <?php
        endforeach;
    endif;
    ?>
</div>
