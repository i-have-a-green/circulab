<?php
/**
 * Block Name: Bloc User
 */
 ?>
<div class="bloc bloc-user-container  display-33-33-33">
    <?php
    $users = get_field('users');
    if ( !$users ) :
    ?>
        <em>Renseigner les blocs ...</em>
    <?php
    else :

        foreach ($users as $key => $id) :
            $id_user = $id["user"];
            $image = get_the_author_meta("image", $id_user);
            ?>
            <div class="blocUSer">

                <div class="picto userPicto ">
                    <?php if( $image ):
                        echo '<div class="userThumbnailContainer">';
                        echo wp_get_attachment_image($image, "thumbnail" );
                        echo '</div>';
                    endif;?>
                    <?php if( get_the_author_meta("user_url", $id_user) ):?>
                        <a href="<?php echo get_the_author_meta("user_url", $id_user);?>" class="userLinkedin" target="_blank"></a>
                    <?php endif;?>
                </div><!-- div.userPicto -->

                <div class="blocPadding">
                  <p class="fontTitle isGreen userJob"><?php _e("Authors", "circulab");?></p>
                  <div class="separator hasGreenBg"></div>
                  <h2 class="title fontTitle userTitle"></span><?php echo get_the_author_meta("user_firstname", $id_user).' '.get_the_author_meta("user_lastname", $id_user); ?></h2>
                  <!--<p><?php echo get_the_author_meta("job", $id_user);?></p>-->
                  <?php echo get_the_author_meta("user_description", $id_user); ?>
                </div><!-- div.blocPadding -->

            </div>
            <?php
        endforeach;
    endif;
    ?>
</div>
