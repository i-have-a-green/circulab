<?php
/**
 * Block Name: Bloc post
 */
 ?>
<div class="bloc-post-container">
    <h2 class="titleSection fontTitle"><?php _e("Blog", "circulab");?></h2>
    <?php
    $args = array(
        'posts_per_page' => 1,
        'post_type'   => "post",
        'post_status' => 'publish',
        'order'	=> 'DESC',
    );
    query_posts($args);
    ?>


      <?php
      if ( have_posts() ) :
          while (have_posts()) : the_post();
              get_template_part('template-parts/archive', 'post');
          endwhile;
      ?>
      <?php
      else :
          _e("Aucun contenu", "arpdl");
          $no_results = true;
      endif;
      wp_reset_query();
      ?>

    <div class="blogButtonContainer">
      <a href="<?php the_permalink( get_field("archive_post_".weglot_get_current_language(), "option") )?>" class="button buttonBlue"><?php _e("All the articles", "circulab");?></a>
    </div>
</div>
