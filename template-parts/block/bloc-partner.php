<?php
/**
 * Block Name: Bloc Partenaire
 */
 ?>

<div class="bloc-partner-container bloc">
    <?php
    $partners = get_field('partners');
    if ( !$partners ) :
    ?>
        <em>Renseigner les partenaires ...</em>
    <?php
    else :
        wp_register_script('flickity', 'https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js', false, false, 'all');
        wp_enqueue_script('flickity');
        echo '<h2 class="title fontTitle isPink partnerTitle">'.get_field("title").'</h2>';
    ?>

    <div class="carousel" data-flickity='{ "wrapAround": true, "pageDots": false }'>

    <?php

        foreach ($partners as $key => $partner) :
            $image = $partner["image"];
            $size = 'partner';
            if( $image ) {
                echo wp_get_attachment_image($image, $size );
            }
        endforeach;
    endif;
    ?>

  </div>

</div>
