<?php
/**
 * Template part for displaying post in single.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Susty
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('wrapper'); ?>>

	<div class="bloc blogCard singleBlogPost">

		<div class="blocPadding">
			<h2 class="titleSection fontTitle"><?php the_title();?></h2>
			<div class="separator hasBlueBg"></div>
		</div>

		<div class="blogPicture blogPictureBig">
			<?php the_post_thumbnail("large");?>
		</div>

		<div class="blocPadding">

			<div class="entry-meta">
				<?php
				$time_string = 'Le <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
				echo sprintf( $time_string,
					esc_attr( get_the_date( DATE_W3C ) ),
					esc_html( get_the_date() ),
					esc_attr( get_the_modified_date( DATE_W3C ) ),
					esc_html( get_the_modified_date() )
				);
				echo " par ".esc_html( get_the_author() );
				?>
			</div><!-- .entry-meta -->

		</div>
	</div>
	<?php the_content();?>
</article><!-- #post-<?php the_ID(); ?> -->
