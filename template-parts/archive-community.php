<?php
/**
 * Template part for displaying event bloc (archive and gutenberg bloc)
 *
 */

?>

<div class="bloc user blocSingleUSer <?php //if(is_user_logged_in()): ?> community-connect-user<?php //endif;?>" data-modal="user-connect-<?php echo $user_id;?>">
	<div class="entry-meta">

		<div class="picto userPicto">
			<?php
			if(get_field("image", "user_".$user_id)){
				$image = get_field("image", "user_".$user_id);
				$size = 'thumbnail';
				if( $image ) { 
					echo '<div class="userThumbnailContainer">';
					echo wp_get_attachment_image( $image, $size );
					echo '</div>';
				}
			}
			else{
				echo '<div class="userThumbnailContainer">';
				echo get_avatar( $user_id, 32 );
				echo '</div>';
			}
			?>
			<?php if( get_the_author_meta("user_url", $user_id) ):?>
				<a href="<?php echo get_the_author_meta("user_url", $user_id);?>" target="_blank" class="userLinkedin"></a>
			<?php endif;?>

		</div><!-- .userPicto -->

		<div class="blocPadding">

			<h2 class="title fontTitle userTitle"><?php echo get_the_author_meta("user_firstname", $user_id).' '.get_the_author_meta("user_lastname", $user_id); ?></h2>
			<div class="separator hasGreenBg"></div>
			<p><?php echo get_the_author_meta("area", $user_id);?></p>
			<?php
				if($skills = get_field("skills", "user_".$user_id)):
					$first = true;
					foreach($skills as $skill):
						$skillTerm = get_term_by('id', $skill, 'skill');
						echo ($first)?'':', ';$first = false;// pour ajouter une virgule entre les skills
						echo $skillTerm->name;
					endforeach;
				endif;
			?>

		</div><!-- .blocPadding -->

	</div><!-- .entry-meta -->

</div>

<?php //if(is_user_logged_in()): ?>
	<div class="modal-user-connect" id="user-connect-<?php echo $user_id;?>">
		<button class="close-modal" data-modal="user-connect-<?php echo $user_id;?>">X</button>
		<div class="modal-user-content">
			<div class="picto userPicto">

				<?php
				if(get_field("image", "user_".$user_id)){
					$image = get_field("image", "user_".$user_id);
					$size = 'thumbnail';
					if( $image ) {
						echo '<div class="userThumbnailContainer">';
						echo wp_get_attachment_image( $image, $size );
						echo '</div>';
					}
				}
				else{
					echo '<div class="userThumbnailContainer">';
					echo get_avatar( $user_id, 32 );
					echo '</div>';
				}
				?>
				<?php if( get_the_author_meta("user_url", $user_id) ):?>
					<a href="<?php echo get_the_author_meta("user_url", $user_id);?>" target="_blank" class="userLinkedin"></a>
				<?php endif;?>
			</div><!-- .userPicto -->
			<div class="modal-user-coordonnee">
				<h2 class="title fontTitle userTitle"><?php echo get_the_author_meta("user_firstname", $user_id).' '.get_the_author_meta("user_lastname", $user_id); ?></h2>
				<hr>
				<!-- zone -->
				<p>
				<?php
					if($zones = get_field("continent", "user_".$user_id)):
						_e("Zone", "circulab");?> : <b><?php
						$first = true;
						foreach($zones as $zone):
							$zoneTerm = get_term_by('id', $zone, 'continent');
							echo ($first)?'':', ';$first = false;
							echo $zoneTerm->name;
						endforeach;
					endif;
				?></b></p>

				<!-- country et location -->
				<!-- Country X -->
				<?php
					$countrys = get_field("country", "user_".$user_id);
					if(!empty($countrys)):
						?><p><?php
						_e("Country", "circulab");?> : <b><?php
							$first = true;
							foreach($countrys as $country):
								$countryTerm = get_term_by('id', $country, 'zone');
								echo ($first)?'':', ';$first = false;
								echo $countryTerm->name;
							endforeach;
						?></b></p><?php
					endif;
				?>
				<!-- Location -->
				<?php $location = get_field("area", "user_".$user_id); ?>
				<?php if(!empty($location)):?>
					<p>
						<?php _e("Location", "circulab");?> : 
						<b><?php echo $location;?></b>
					</p>
				<?php endif;?>	


				<!-- Coordonnées -->
				<!-- mail -->
				<?php if( get_the_author_meta("email", $user_id) ):?>
					<p>
						<span><?php _e("Email", "circulab");?> : <?php echo get_the_author_meta("email", $user_id);?></span>
					</p>
				<?php endif;?>
				<!-- phone -->
				<?php $phone = get_field("phone", "user_".$user_id); ?>
				<?php if(!empty($phone)):?>
					<p>
						<span><?php _e("Phone", "circulab") ;?> : <?php echo $phone;?></span>	
					</p>
				<?php endif;?>	

				<!-- Latest clients -->
				<!--<?php $latest_client = get_field("customer-structures", "user_".$user_id); ?>
				<?php if(!empty($latest_client)):?>
					<p>
						<?php _e("Latest clients", "circulab");?> : 
						<b><?php echo $latest_client;?></b>
					</p>
				<?php endif;?>	-->

			</div>

			<div class="modal-user-col-left">

				<!--<?php if( get_the_author_meta("training", $user_id) ):?>
					<p><?php _e("Initial training", "circulab");?><br><b><?php echo get_the_author_meta("training", $user_id);?></b></p>
				<?php endif;?>-->
				<?php if( get_the_author_meta("job", $user_id) ):?>
					<p><?php _e("Current job", "circulab");?><br><b><?php echo get_the_author_meta("job", $user_id);?></b></p>
				<?php endif;?>
				<?php if( get_the_author_meta("company", $user_id) ):?>
					<p><?php _e("Current company", "circulab");?><br><b><?php echo get_the_author_meta("company", $user_id);?></b></p>
				<?php endif;?>

				<p>
				<?php
					_e("Skills", "circulab");?><br><b><?php
					if($skills = get_field("skills", "user_".$user_id)):
						$first = true;
						foreach($skills as $skill):
							$skillTerm = get_term_by('id', $skill, 'skill');
							echo ($first)?'':', ';$first = false;// pour ajouter une virgule entre les skills
							echo $skillTerm->name;
						endforeach;
					endif;
				?></b></p>

			</div>

		
			<div class="modal-user-col-right">
				<!-- Paragraph presentation -->
				<?php $presentation = get_field("paragraph_presentation", "user_".$user_id) ;?>
				<?php if(!empty($presentation)):?>
					<p>
						<?php _e("Presentation", "circulab");?> : 
						<b><?php echo $presentation;?></b>
					</p>
				<?php endif;?>	

				<?php if( get_the_author_meta("customer-type", $user_id) ):?>
					<p><?php _e("Latest clients", "circulab");?><br><b><?php echo get_the_author_meta("customer-type", $user_id);?></b></p>
				<?php endif;?>

				<!--<?php if( get_the_author_meta("value", $user_id) ):?>
					<p><?php _e("Values", "circulab");?><br><b><?php echo get_the_author_meta("value", $user_id);?></b></p>
				<?php endif;?>

				<?php if( get_the_author_meta("meetup-organisation", $user_id) ):?>
					<p><?php _e("Meetup organization", "circulab");?><br><b><?php echo get_the_author_meta("meetup-organisation", $user_id);?></b></p>
				<?php endif;?>

				<?php if( get_the_author_meta("mastermind-organisation", $user_id) ):?>
					<p><?php _e("Mastermind organization", "circulab");?><br><b><?php echo get_the_author_meta("mastermind-organisation", $user_id);?></b></p>
				<?php endif;?>

				<?php if( get_the_author_meta("withaa-training", $user_id) ):?>
					<p><?php _e("Values", "circulab");?><br><b><?php echo get_the_author_meta("withaa-training", $user_id);?></b></p>
				<?php endif;?>

				<?php if( get_the_author_meta("value", $user_id) ):?>
					<p><?php _e("Training co-designed with Circulab", "circulab");?><br><b><?php echo get_the_author_meta("value", $user_id);?></b></p>
				<?php endif;?>-->
			</div>
		</div>
	</div><!-- .modal-user-connect -->
	
<?php //endif;?>