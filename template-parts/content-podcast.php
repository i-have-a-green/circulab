<?php
/**
 * Template part for displaying podcast
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('wrapper singlePodcastContainer'); ?>>

	<div class="bloc podcastBg podcast-container" style="background-image:url(<?php //the_post_thumbnail_url();?>">

		<div class="podcastLinkContainer">

			<?php if(get_field("spotify")):?>
				<a class="podcastLink podcastLinkSpotify "href="<?php the_field("spotify");?>" target="_blank" title="Ouverture du podcast sur spotify dans un autre onglet">Spotify</a>
			<?php endif;?>
			<?php if(get_field("apple")):?>
			<a class="podcastLink podcastLinkApple" href="<?php the_field("apple");?>" target="_blank" title="Ouverture du podcast sur apple podcast dans un autre onglet">Apple</a>
			<?php endif;?>
			<!--Je n'ai pas trouvé le 3em ! -->

		</div>

		<div class="picto pictoPosition-default podcastPicto">
			<!-- Poda Icon Svg -->
			<svg xmlns="http://www.w3.org/2000/svg" width="38.302" height="36.071" viewBox="0 0 38.302 36.071">
			  <g id="podcast" transform="translate(5711.591 -687.759)">
			    <path id="Tracé_3088" data-name="Tracé 3088" d="M-5668.313,723.328a4.9,4.9,0,0,1-4.894-4.894v-5.626a4.9,4.9,0,0,1,4.894-4.894,4.9,4.9,0,0,1,4.894,4.894v5.626A4.9,4.9,0,0,1-5668.313,723.328Zm0-14.18a3.663,3.663,0,0,0-3.659,3.66v5.626a3.663,3.663,0,0,0,3.659,3.659,3.663,3.663,0,0,0,3.659-3.659v-5.626A3.664,3.664,0,0,0-5668.313,709.148Z" transform="translate(-12.65 -6.642)"/>
			    <path id="Tracé_3089" data-name="Tracé 3089" d="M-5669.68,731.365a7.578,7.578,0,0,1-7.674-7.46.618.618,0,0,1,.618-.617.618.618,0,0,1,.617.617,6.343,6.343,0,0,0,6.439,6.226,6.342,6.342,0,0,0,6.438-6.226.618.618,0,0,1,.618-.617.618.618,0,0,1,.617.617A7.577,7.577,0,0,1-5669.68,731.365Z" transform="translate(-11.283 -11.709)"/>
			    <path id="Tracé_3090" data-name="Tracé 3090" d="M-5666.211,739.008a.617.617,0,0,1-.617-.617V734.43a.618.618,0,0,1,.617-.617.618.618,0,0,1,.618.617v3.961A.618.618,0,0,1-5666.211,739.008Z" transform="translate(-14.752 -15.178)"/>
			    <path id="Tracé_3091" data-name="Tracé 3091" d="M-5664.689,740.955h-5.988a.617.617,0,0,1-.617-.617.617.617,0,0,1,.617-.617h5.988a.618.618,0,0,1,.618.617A.617.617,0,0,1-5664.689,740.955Z" transform="translate(-13.281 -17.125)"/>
			    <path id="Tracé_3092" data-name="Tracé 3092" d="M-5687.525,715.333h-19.92a4.15,4.15,0,0,1-4.146-4.145V691.9a4.15,4.15,0,0,1,4.146-4.145h22.934a4.15,4.15,0,0,1,4.146,4.145v9.984a.617.617,0,0,1-.618.617.617.617,0,0,1-.617-.617V691.9a2.914,2.914,0,0,0-2.911-2.91h-22.934a2.914,2.914,0,0,0-2.911,2.91v19.284a2.914,2.914,0,0,0,2.911,2.91h19.92a.617.617,0,0,1,.617.617A.618.618,0,0,1-5687.525,715.333Z"/>
			    <path id="Tracé_3093" data-name="Tracé 3093" d="M-5680.984,699.7h-29.99a.617.617,0,0,1-.617-.617.617.617,0,0,1,.617-.617h29.99a.618.618,0,0,1,.618.617A.617.617,0,0,1-5680.984,699.7Z" transform="translate(0 -3.527)"/>
			    <path id="Tracé_3094" data-name="Tracé 3094" d="M-5699.233,694.951a1.728,1.728,0,0,1-1.726-1.726,1.728,1.728,0,0,1,1.726-1.726,1.728,1.728,0,0,1,1.725,1.726A1.727,1.727,0,0,1-5699.233,694.951Zm0-2.216a.491.491,0,0,0-.491.491.491.491,0,0,0,.491.491.491.491,0,0,0,.491-.491A.491.491,0,0,0-5699.233,692.735Z" transform="translate(-3.504 -1.233)"/>
			    <path id="Tracé_3095" data-name="Tracé 3095" d="M-5706.659,694.928a1.728,1.728,0,0,1-1.726-1.726,1.728,1.728,0,0,1,1.726-1.726,1.728,1.728,0,0,1,1.727,1.726A1.728,1.728,0,0,1-5706.659,694.928Zm0-2.216a.492.492,0,0,0-.491.491.492.492,0,0,0,.491.491.492.492,0,0,0,.491-.491A.492.492,0,0,0-5706.659,692.712Z" transform="translate(-1.057 -1.225)"/>
			    <path id="Tracé_3096" data-name="Tracé 3096" d="M-5691.808,694.975a1.728,1.728,0,0,1-1.726-1.726,1.728,1.728,0,0,1,1.726-1.726,1.727,1.727,0,0,1,1.726,1.726A1.728,1.728,0,0,1-5691.808,694.975Zm0-2.216a.491.491,0,0,0-.49.491.492.492,0,0,0,.49.491.491.491,0,0,0,.491-.491A.491.491,0,0,0-5691.808,692.759Z" transform="translate(-5.951 -1.241)"/>
			  </g>
			</svg>
		</div>
		<h2 class="title fontTitle"><?php the_title();?></h2>
		<div class="separator hasGreenBg"></div>

		<?php
		if(get_field('podcastFile')):
			$attr = array(
				'src'      => get_field('podcastFile'),
				'loop'     => '',
				'autoplay' => '',
				'preload' => 'none'
				);
			echo wp_audio_shortcode( $attr );
		elseif(get_field("podcastURL")):
			$attr = array(
				'src'      => get_field('podcastURL'),
				'loop'     => '',
				'autoplay' => '',
				'preload' => 'none'
				);
			echo wp_audio_shortcode( $attr );
		endif;
		?>


		<?php /* the_post_thumbnail("large"); */?>
		<div class="entry-meta">
			<?php
			/*$time_string = 'Le <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
			echo sprintf( $time_string,
				esc_attr( get_the_date( DATE_W3C ) ),
				esc_html( get_the_date() ),
				esc_attr( get_the_modified_date( DATE_W3C ) ),
				esc_html( get_the_modified_date() )
			);*/
			?>
		</div><!-- .entry-meta -->
	</div>

	<?php the_content();?>

</article><!-- #post-<?php the_ID(); ?> -->
