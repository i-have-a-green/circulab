<?php
/**
 * Template part for displaying event bloc (archive and gutenberg bloc)
 *
 */

?>

	<div class="bloc eventCard singleEvent">
		<div class="entry-meta">

				<div class="eventHeader">
				<p class="title fontTitle heightAuto">
					<?php
						if(get_field("type", $post->ID) == "demo"):
							_e("Demo", "circulab");
						elseif(get_field("type", $post->ID) == "conference"):
							_e("Conference", "circulab");
						else:
							_e("Training", "circulab");
						endif;
					?>
				</p>
				<div class="separator hasBlueBg"></div>
				<?php $term = get_the_terms( $post->ID, "lieu" );?>
				<p class="eventInfo heightAuto"><?php echo date_i18n( "d F", strtotime(get_field("date_event", $post->ID)))?> - <?php echo (isset($term[0]->name))?$term[0]->name:'';?></p>
				<h2 class="eventName fontTitle"><?php the_title();?></h2>
			</div>

			<div class="backgroundEvent">
				<?php

				if(isset($term[0]) && get_field("image", "lieu_".$term[0]->term_id)){
					$image = get_field("image", "lieu_".$term[0]->term_id);
					$size = 'bloc-half';
					if( $image ) {
						echo wp_get_attachment_image( $image, $size );
					}
				}
				else{
					the_post_thumbnail( $size = 'bloc-half', $post->ID );
				}
				?>
			</div>
			<?php if(!empty(get_field("link", $post->ID))):?>
				<div class="ctaContainer">
					<a href="<?php the_field("link", $post->ID);?>" class="button buttonBlue cta" target="_blank"><?php _e("Register", "circulab");?></a>
				</div>
			<?php endif;?>
		</div><!-- .entry-meta -->
	</div>
