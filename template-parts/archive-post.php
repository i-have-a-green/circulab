<?php
/**
 * Template part for displaying post in archive.php
 *
 */

?>

<article id="post-<?php the_ID(); ?>" class="singlePost">

	<div class="bloc blogCard">

		<div class="blocPadding">
			<a class="title fontTitle" href="<?php the_permalink();?>"><h2><?php the_title();?></h2></a>
			<div class="separator hasBlueBg"></div>
		</div>

		<div class="blogPicture">
			<?php the_post_thumbnail("pano"); //mettre en display none suivant le cas (bloc-post ou archive-post) ?>
			<?php the_post_thumbnail("bg");//mettre en display none suivant le cas (bloc-post ou archive-post) ?>
		</div>

		<div class="blocPadding  blogFooter">

			<div class="entry-meta">
				<?php
				$time_string = 'Le <time class="entry-date published updated" datetime="%1$s">%2$s</time>';
				echo sprintf( $time_string,
					esc_attr( get_the_date( DATE_W3C ) ),
					esc_html( get_the_date() ),
					esc_attr( get_the_modified_date( DATE_W3C ) ),
					esc_html( get_the_modified_date() )
				);
				?>
			</div><!-- .entry-meta -->

					<a href="<?php the_permalink();?>" class="buttonBlog"><?php _e("Read the article", "circulab");?></a>

		</div>

	</div>
</article><!-- #post-<?php the_ID(); ?> -->
