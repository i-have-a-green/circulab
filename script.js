document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById('form-community')) {

        document.getElementById('form-community-zone').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-continent').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-skill').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-type-of-company').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-certification').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

       /* document.getElementById('form-community-mastermind').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-meetup').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-customer').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });*/

        



        var els = document.getElementsByClassName("blocSingleUSer");
        Array.prototype.forEach.call(els, function(el) {
            el.addEventListener("click", function(e) {
                //e.preventDefault();
                modalID = document.getElementById(el.dataset.modal);
                modalID.classList.add("active");
                document.getElementById("BGmodal").classList.add("active");
            });
        });

        var closeEls = document.getElementsByClassName("close-modal");
        Array.prototype.forEach.call(closeEls, function(el) {
            el.addEventListener("click", function(e) {
                e.preventDefault();
                modal = document.getElementById(el.dataset.modal);
                if (modal.classList.contains("active")) modal.classList.remove("active");
                document.getElementById("BGmodal").classList.remove("active");
            });
        });

    }

    if (document.getElementById('loadMoreUsers')) {
        el = document.getElementById('loadMoreUsers');
        el.addEventListener("click", function() {
            var comUser = document.getElementById('community-users');
            var offsetBottom = comUser.offsetTop + comUser.offsetHeight;
            var tab = [];
            for(var i = nb_users_display; i < (pasUser + nb_users_display); i++){
                if(display_users[i]){
                    tab.push(display_users[i]); 
                }
            }
            nb_users_display = nb_users_display + pasUser;
            var formData = new FormData();
            formData.append("display_users", JSON.stringify(tab));
            xhr = new XMLHttpRequest();
            xhr.open('POST', resturl+'display_users' , true);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    //console.log(xhr.response);
                    document.getElementById('community-users').insertAdjacentHTML('beforeend', xhr.response);
                    var els = document.getElementsByClassName("blocSingleUSer");
                    Array.prototype.forEach.call(els, function(el) {
                        el.addEventListener("click", function(e) {
                            //e.preventDefault();
                            modalID = document.getElementById(el.dataset.modal);
                            modalID.classList.add("active");
                            document.getElementById("BGmodal").classList.add("active");
                        });
                    });
                    var closeEls = document.getElementsByClassName("close-modal");
                    Array.prototype.forEach.call(closeEls, function(el) {
                        el.addEventListener("click", function(e) {
                            e.preventDefault();
                            modal = document.getElementById(el.dataset.modal);
                            if (modal.classList.contains("active")) modal.classList.remove("active");
                            document.getElementById("BGmodal").classList.remove("active");
                        });
                    });

                    window.scrollTo(0, (offsetBottom - 80));

                }
            };
            xhr.send(formData); 

        });
    }
});
document.addEventListener('DOMContentLoaded', function () {
    if (document.getElementById('form-contact')) {
        document.getElementById('form-contact').addEventListener('submit', function (e) {
            e.preventDefault();
            document.getElementById('contact_send').disabled = true;
            var form = document.forms.namedItem("form-contact");
            var formData = new FormData(form);
            xhr = new XMLHttpRequest();
            xhr.open('POST', ajaxurl, true);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    document.getElementById('contact_send').disabled = false;
                    document.getElementById("contResponse").innerHTML = xhr.response;
                    window.scrollTo({
                        top: document.getElementById("content").offsetTop,
                        behavior: 'smooth',
                    });
                    document.getElementById("form-contact").classList.add("response");
                }
            }; 
            xhr.send(formData);
        });
    }
});
document.addEventListener('DOMContentLoaded', function () {
    if (document.getElementById('form-event')) {
        document.getElementById('form-event').addEventListener('submit', function (e) {
            e.preventDefault();
            document.getElementById('event_send').disabled = true;
            var form = document.forms.namedItem("form-event");
            var formData = new FormData(form);
            xhr = new XMLHttpRequest();
            xhr.open('POST', resturl+'addevent', true);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    document.getElementById('event_send').disabled = false;
                    document.getElementById("contResponse").innerHTML = xhr.response;
                    window.scrollTo({
                        top: document.getElementById("content").offsetTop,
                        behavior: 'smooth',
                    });
                    document.getElementById("form-event").classList.add("response"); 
                }
            }; 
            xhr.send(formData);
        });


        var radios = document.querySelectorAll('input[type=radio][name="lieu"]');
        Array.prototype.forEach.call(radios, function(radio) {
         radio.addEventListener('change', function(){
             if(this.value == 0){
                 document.getElementById("newCity").style.display = "block";
                 document.getElementById("city").attributes.required = "required";
                 document.getElementById("file").attributes.required = "required";
             }
             else{
                document.getElementById("newCity").style.display = "none";
                document.getElementById("city").attributes.required = "";
                document.getElementById("file").attributes.required = "";
             }
         });
        });

    }
});

    
/*    if(document.getElementById('map-community')){

        // On initialise la latitude et la longitude de Paris (centre de la carte)
        var lat = 48.852969;
        var lon = 2.349903;
        var macarte = null;

        window.onload = function(){
            // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
            macarte = L.map('map-community').setView([lat, lon], 2);
            // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
            L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
                // Il est toujours bien de laisser le lien vers la source des données
                attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
                minZoom: 1,
                maxZoom: 20
            }).addTo(macarte);


            
            var url = new URL(window.location.href);
            var zone = "";
            if(url.searchParams.has("zone") === true){
                zone = url.searchParams.get("zone");
            }
            L.geoJSON(myLines, {
                style: function(feature) {
                    switch (feature.properties.CONTINENT) {
                        case zone: return {   "color": "#011BC8",
                                                "weight": 1,
                                                "opacity": 0.75
                                            };
                        default : return {  "color": "#999",
                                            "weight": 0,
                                            "opacity": 0.25
                                        };
                    } 
                }
            }).addTo(macarte); 
        };
    } */
document.addEventListener("DOMContentLoaded", function(event) {

    /*document.getElementById("link-menu").addEventListener("click", function(e) { 
        e.preventDefault();
        document.getElementById("cont-menu-full-screen").classList.toggle("openMenu");
    });
    document.getElementById("cont-menu-full-screen").addEventListener("click", function(e) {
        document.getElementById("cont-menu-full-screen").classList.remove("openMenu");
    });*/

});

// ouverture - fermeture du menu mobile - source @ https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_toggle_class_crossbrowser

// Classlist #menuConnect
function toggleMenuConnect() {
  var element = document.getElementById("menuConnect");

  if (element.classList) {
    element.classList.toggle("menuConnectOpen");
  } else {
    var classes = element.className.split(" ");
    var i = classes.indexOf("menuConnectOpen");

    if (i >= 0)
      classes.splice(i, 1);
    else
      classes.push("menuConnectOpen");
      element.className = classes.join(" ");
  }
}

// ouverture - fermeture du menu mobile - source @ https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_toggle_class_crossbrowser

// Classlist #menuMobile
function toggleMenuMobile() {
  var element = document.getElementById("menuMobile");
  var topbar = document.getElementById("menuTopbarLogo");

  if (element.classList) {
    element.classList.toggle("menuMobileOpen");
    topbar.classList.toggle("menuMobileOpen");
  } else {
    var classes = element.className.split(" ");
    var i = classes.indexOf("menuMobileOpen");

    if (i >= 0)
      classes.splice(i, 1);
    else
      classes.push("menuMobileOpen");
      element.className = classes.join(" ");
  }
}



document.addEventListener("DOMContentLoaded", function(event) {
    
    //ouverture de la modal
    var els = document.getElementsByClassName("buttonModal");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) { 
            e.preventDefault(); 
            modal = document.getElementById(el.dataset.idModal);
            modal.classList.add("active");
            document.getElementById("BGmodal").classList.add("active");
        });
    });

    //fermeture de la modal 
    var closeModals = document.getElementsByClassName("closeModal");
    Array.prototype.forEach.call(closeModals, function(closeModal) {
        closeModal.addEventListener("click", function(e) { 
            e.preventDefault();

            var modals = document.getElementsByClassName("modal");
            Array.prototype.forEach.call(modals, function(modal) {
                modal.classList.remove("active");
                document.getElementById("BGmodal").classList.remove("active");
            });
        });
    });
    
}); 
// ouverture - fermeture du menu mobile - source @ https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_toggle_class_crossbrowser

// Classlist #menuConnect
function showMoreItem() {
  var element = document.getElementById("archiveContainer");

  if (element.classList) {
    element.classList.toggle("showMoreItems");
  } else {
    var classes = element.className.split(" ");
    var i = classes.indexOf("showMoreItems");

    if (i >= 0)
      classes.splice(i, 1);
    else
      classes.push("showMoreItems");
      element.className = classes.join(" ");
  }
}

var slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  //var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1;}
  if (n < 1) {slideIndex = slides.length;}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
//   for (i = 0; i < dots.length; i++) {
//       dots[i].className = dots[i].className.replace(" active", "");
//   }
  if(slides[slideIndex-1]){
    slides[slideIndex-1].style.display = "block";
  }
  
//   dots[slideIndex-1].className += " active";
}
window.addEventListener("scroll", myFunction);

function myFunction() {
    if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
        document.getElementById("masthead").className = "hasShadow";
    } else {
        document.getElementById("masthead").className = "";
    }
}

document.addEventListener('DOMContentLoaded', function () {
    if (document.getElementById('form-user')) {
        document.getElementById('form-user').addEventListener('submit', function (e) {
            e.preventDefault();
            document.getElementById('user_send').disabled = true;
            var form = document.forms.namedItem("form-user");
            var formData = new FormData(form);
            xhr = new XMLHttpRequest();
            xhr.open('POST', resturl+'updateuser', true);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    document.getElementById('user_send').disabled = false;
                    document.getElementById("contResponse").innerHTML = xhr.response;
                    window.scrollTo({
                        top: document.getElementById("content").offsetTop,
                        behavior: 'smooth',
                    });
                    document.getElementById("form-user").classList.add("response"); 
                }
            }; 
            xhr.send(formData);
        });
    }
}); 