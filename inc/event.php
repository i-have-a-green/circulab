<?php
/*
* traitement du post du form de Event
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'addevent',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wpgreen_formEvent',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
});

function wpgreen_formEvent(){


		global $wpdb;

		/*$upload_file_text = "";
		if(isset($_FILES['file'])){
			$maintenant = date("d-m-Y_H:i:s");
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/file-form/';
			@mkdir($uploaddirimg, 0755);
			$uploadfile = $uploaddirimg . $maintenant . '-'.basename($_FILES['file']['name']);
			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                $filename = $upload_dir['baseurl'].'/file-form/' . $maintenant . '-'.basename($_FILES['file']['name']);
                $upload_file_text = "Fichier : ".$filename;
			}
		}*/

		
        
		$post['post_type']   = 'event';
		$post['post_status'] = 'publish';
		$post['post_title'] = sanitize_text_field($_POST['title']);
		$post['post_content'] = '';
		        
		$p = wp_insert_post( $post, true );

		if((int)sanitize_text_field($_POST['lieu']) > 0 ){
			wp_set_post_terms( $p, array((int)sanitize_text_field($_POST['lieu'])), 'lieu' );	
		}
		else{
			$term = wp_insert_term(
				sanitize_text_field($_POST['city']),   // the term 
				'lieu', // the taxonomy
			);

			wp_set_post_terms( $p, $term["term_id"], 'lieu' );
			if(isset($_FILES['file'])){
				$upload_dir   = wp_upload_dir();
				$uploaddirimg = $upload_dir['basedir'].'/';
				if(!is_dir($uploaddirimg)){
					mkdir($uploaddirimg, 0755);
				}		
				$filename = $uploaddirimg . basename($_FILES['file']['name']);
				if (move_uploaded_file($_FILES['file']['tmp_name'], $filename)) {
					$wp_filetype = wp_check_filetype($filename, null );
					$attachment = array(
						'guid'           => $wp_upload_dir['url'] . '/'.basename($_FILES['file']['name']),
						'post_mime_type' => $wp_filetype['type'],
						//'post_parent' => $product_id,
						'post_title' => preg_replace('/\.[^.]+$/', '', basename($_FILES['file']['name'])),
						'post_content' => '',
						'post_status' => 'inherit'
					);
					$attachment_id = wp_insert_attachment( $attachment, $filename);
					$attach_data = wp_generate_attachment_metadata( $attachment_id, $filename );
					wp_update_attachment_metadata( $attachment_id, $attach_data );
					update_field('image', $attachment_id, "lieu_".$term["term_id"]);

				}
			}	
		
		}
		

		
		update_field( 'type', sanitize_text_field($_POST['type']), $p );
		update_field( 'date_event', sanitize_text_field($_POST['date_event']), $p );
		update_field( 'date_event_end', sanitize_text_field($_POST['date_event_end']), $p );
		update_field( 'date_texte', sanitize_text_field($_POST['date_texte']), $p );
		$term = get_term( (int)sanitize_text_field($_POST['lieu']), 'lieu' );
		update_field( 'zone', $term->name, $p );
		update_field( 'link', sanitize_text_field($_POST['link']), $p );

        /*if(!empty($upload_file_text)){
            contactInsertAttachment($filename, $p);
        }*/

		echo __("The team of, ","circulab") . get_bloginfo('name') .  __(" examine your event proposal", "circulab");
	
	//wp_die();
}
/*
function contactInsertAttachment($filename, $p){
	$filetype = wp_check_filetype( basename( $filename ), null );

	// Get the path to the upload directory.
	$wp_upload_dir = wp_upload_dir();

	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);

	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $filename, $p );
    update_post_meta( $p, 'attachment', $attach_id );//image à la une
    update_post_meta( $p, 'fileAttachment', $wp_upload_dir['basedir'].'/file-form/'. basename( $filename ) );//image à la une
}*/

function post_published_notification( $post_id, $post ) {
    $author = $post->post_author; /* Post author ID. */
    $name = get_the_author_meta( 'display_name', $author );
    $email = get_the_author_meta( 'user_email', $author );
    $title = $post->post_title;
    $permalink = get_permalink( $post_id );
    $edit = get_edit_post_link( $post_id, '' );
    $to[] = sprintf( '%s <%s>', $name, $email );
    $subject = sprintf( 'Published: %s', $title );
    $message = sprintf ('Congratulations, %s! Your article "%s" has been published.' . "\n\n", $name, $title );
    $message .= sprintf( 'View: %s', $permalink );
    $headers[] = '';
    wp_mail( $to, $subject, $message, $headers );
}
add_action( 'publish_event', 'post_published_notification', 10, 2 );