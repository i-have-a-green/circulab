<?php
 
add_action('wp_enqueue_scripts', 'site_scripts', 999);
function site_scripts()
    {
    global $wp_styles;

    wp_deregister_script('jquery');

    wp_deregister_script('underscore');

    wp_register_script('leaflet-js', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.js', array(), '1.4.0', true);
    wp_register_script('coordContinent-js', get_stylesheet_directory_uri() . '/coordContinent.js', array('leaflet-js'), false, 'all');

	wp_register_style('leaflet', 'https://unpkg.com/leaflet@1.4.0/dist/leaflet.css', array(), '1.4.0', 'all');
	//wp_register_script('markercluster', 'https://unpkg.com/leaflet.markercluster@1.1.0/dist/leaflet.markercluster.js', array('leaflet-js'), '1.1.0', 'all');
	//wp_register_script('wpgreen-map-js', plugin_dir_url( __FILE__ ) . 'map.js', array('leaflet-js'), '1.0.0', 'all');
	if (is_page_template( 'template/page-community.php' ) ) {
		wp_enqueue_style('leaflet');
        wp_enqueue_script('leaflet-js');
        wp_enqueue_script('coordContinent-js');
		//wp_enqueue_script('markercluster');
        //wp_enqueue_script('wpgreen-map-js');
        wp_register_script('script', get_stylesheet_directory_uri() . '/script.js', array('coordContinent-js'), false, 'all');
        wp_enqueue_script('script');
    } 
    else{
        wp_register_script('script', get_stylesheet_directory_uri() . '/script.js', false, false, 'all');
        wp_enqueue_script('script');
    }

    

    wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    wp_localize_script('script', 'resturl', get_bloginfo('url') . '/wp-json/ihag/');
    
}

function remove_field_user_css()
{
    echo '<style>
    #profile-page tr.user-facebook-wrap,
    #profile-page tr.user-instagram-wrap,
    #profile-page tr.user-myspace-wrap,
    #profile-page tr.user-linkedin-wrap,
    #profile-page tr.user-pinterest-wrap,
    #profile-page tr.user-soundcloud-wrap,
    #profile-page tr.user-tumblr-wrap,
    #profile-page tr.user-youtube-wrap,
    #profile-page tr.user-wikipedia-wrap,
    #profile-page tr.user-twitter-wrap,
    #profile-page tr.user-description-wrap,
    #profile-page tr.user-profile-wrap,
    #profile-page div.yoast-settings,
    #profile-page .user-rich-editing-wrap,
    #profile-page .user-syntax-highlighting-wrap,
    #profile-page .user-admin-color-wrap,
    #profile-page .user-comment-shortcuts-wrap,
    #profile-page .user-admin-bar-front-wrap
    {
        display: none;
    }
    </style>';
}
add_action( 'admin_head-user-edit.php', 'remove_field_user_css' );
add_action( 'admin_head-profile.php',   'remove_field_user_css' );
add_filter( 'ure_show_additional_capabilities_section', '__return_false' );