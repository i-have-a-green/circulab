<?php

/*
 * ACF BLOCKS
 * Source: https://www.advancedcustomfields.com/resources/acf_register_block_type/
 * ----------------------------------------------------------------------------*/
add_action('acf/init', 'acf_init_blocs');
function acf_init_blocs() {
    if( function_exists('acf_register_block_type') ) {
    
        /*
        * Bloc  
        * ----------------------------------------------------------------------------*/
        acf_register_block_type(
        array(
            'name'				    => 'bloc-circulab',
            'title'				    => __('Bloc Circulab'),
            'description'		    => __('Bloc Circulab'),
            'placeholder'		    => __('Bloc Circulab'),
            'render_callback'	    => 'getRenderTemplateHTML',
            'category'			    => 'circulab',
            'mode' => 'edit',
            'icon'				    => 'welcome-write-blog',
            'keywords'			    => array('bloc', 'circulab', 'block')
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'bloc-title',
                'title'				    => __('Bloc titre'),
                'description'		    => __('Bloc titre'),
                'placeholder'		    => __('Bloc titre'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'circulab',
                'mode' => 'edit',
                'icon'				    => 'heading',
                'keywords'			    => array('titre')
                )
            );
    

        /*
        * Bloc  
        * ----------------------------------------------------------------------------*/
        acf_register_block_type(
        array(
            'name'				    => 'bloc-defis',
            'title'				    => __('Bloc Défis'),
            'description'		    => __('Bloc Défis'),
            'placeholder'		    => __('Bloc Défis'),
            'render_callback'	    => 'getRenderTemplateHTML',
            'category'			    => 'circulab',
            'mode' => 'edit',
            'icon'				    => 'welcome-write-blog',
            'keywords'			    => array('bloc', 'défis', 'defis')
            )
        );
    

        /*
        * Bloc  
        * ----------------------------------------------------------------------------*/
        acf_register_block_type(
        array(
            'name'				    => 'bloc-partner',
            'title'				    => __('Bloc Partenaire'),
            'description'		    => __('Bloc Partenaire'),
            'placeholder'		    => __('Bloc Partenaire'),
            'render_callback'	    => 'getRenderTemplateHTML',
            'category'			    => 'circulab',
            'icon'				    => 'groups',
            'mode' => 'edit',
            'keywords'			    => array('bloc', 'partenaire', 'client')
            )
        );

        /*
        * Bloc  
        * ----------------------------------------------------------------------------*/
        acf_register_block_type(
        array(
            'name'				    => 'bloc-event',
            'title'				    => __('Bloc événement'),
            'description'		    => __('Bloc événement'),
            'placeholder'		    => __('Bloc événement'),
            'render_callback'	    => 'getRenderTemplateHTML',
            'category'			    => 'circulab',
            'icon'				    => 'tickets',
            'mode' => 'edit',
            'keywords'			    => array('bloc', 'événement', 'event')
            )
        );

        /*
        * Bloc  
        * ----------------------------------------------------------------------------*/
        acf_register_block_type(
        array(
            'name'				    => 'bloc-book',
            'title'				    => __('Bloc Book'),
            'description'		    => __('Bloc Book'),
            'placeholder'		    => __('Bloc Book'),
            'render_callback'	    => 'getRenderTemplateHTML',
            'category'			    => 'circulab',
            'icon'				    => 'book-alt',
            'mode' => 'edit',
            'keywords'			    => array('bloc', 'Book', 'livre')
            )
        );

        /*
        * Bloc  
        * ----------------------------------------------------------------------------*/
        acf_register_block_type(
            array(
                'name'				    => 'bloc-post',
                'title'				    => __('Bloc Actualité'),
                'description'		    => __('Bloc Actualité'),
                'placeholder'		    => __('Bloc Actualité'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'circulab',
                'mode' => 'edit',
                'icon'				    => 'welcome-write-blog',
                'keywords'			    => array('bloc', 'Actualité', 'news')
            )
        );

        /*
        * Bloc  
        * ----------------------------------------------------------------------------*/
        acf_register_block_type(
            array(
                'name'				    => 'bloc-user',
                'title'				    => __('Bloc Utilisateur'),
                'description'		    => __('Bloc Utilisateur'),
                'placeholder'		    => __('Bloc Utilisateur'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'circulab',
                'mode' => 'edit',
                'icon'				    => 'admin-users',
                'keywords'			    => array('bloc', 'Utilisateur', 'user')
            )
        );

        /*
        * Bloc  
        * ----------------------------------------------------------------------------*/
        acf_register_block_type(
            array(
                'name'				    => 'bloc-paragraphe',
                'title'				    => __('Bloc paragraphe'),
                'description'		    => __('Bloc paragraphe'),
                'placeholder'		    => __('Bloc paragraphe'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'circulab',
                'icon'				    => 'media-document',
                'mode' => 'edit',
                'keywords'			    => array('bloc', 'paragraphe', 'text', 'editeur')
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'bloc-portfolio',
                'title'				    => __('Bloc portfolio'),
                'description'		    => __('Bloc portfolio'),
                'placeholder'		    => __('Bloc portfolio'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'circulab',
                'icon'				    => 'format-gallery',
                'mode' => 'edit',
                'keywords'			    => array('bloc', 'portfolio', 'images')
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'bloc-portfolio-full-width',
                'title'				    => __('Bloc portfolio Full Width'),
                'description'		    => __('Bloc portfolio Full Width'),
                'placeholder'		    => __('Bloc portfolio Full Width'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'circulab',
                'icon'				    => 'format-gallery',
                'mode' => 'edit',
                'keywords'			    => array('bloc', 'portfolio', 'images', 'full', 'width')
            )
        );

        acf_register_block_type(
            array(
                'name'				    => 'bloc-quotes',
                'title'				    => __('Bloc citations - clients'),
                'description'		    => __('Bloc with quotes'),
                'placeholder'		    => __('Bloc quotes from clients'),
                'render_callback'	    => 'getRenderTemplateHTML',
                'category'			    => 'circulab',
                'icon'				    => 'format-quote',
                'mode' => 'edit',
                'keywords'			    => array('bloc', 'quotes', 'clients')
            )
        );


    }
}


function getRenderTemplateHTML($block) {
 // Rendu du bloc
 $slug = str_replace('acf/', '', $block['name']);

 // include a template part from within the "template-parts/block" folder
 if ( file_exists(get_theme_file_path("/template-parts/block/{$slug}.php")) ) :
   include(get_theme_file_path("/template-parts/block/{$slug}.php") );
 endif;
}

// FILTRES LES BLOCS AUTORISES SUR LE SITE
// cf. https://rudrastyh.com/gutenberg/remove-default-blocks.html
// -----------------------------------------------------------------------------
add_filter( 'allowed_block_types', 'nb_allowed_block_types', 10, 2 );
function nb_allowed_block_types($allowed_blocks, $post) {
	$allowed_blocks = array(

		// Blocs spécifiques du theme 
    'acf/bloc-circulab',
    'acf/bloc-paragraphe',
    'acf/bloc-user',
    'acf/bloc-post',
    'acf/bloc-book',
    'acf/bloc-event',
    'acf/bloc-partner',
    'acf/bloc-defis',
    'acf/bloc-portfolio',
    'acf/bloc-title',
    'acf/bloc-portfolio-full-width',
    'acf/bloc-quotes',

    // Blocs communs
    //'core/heading',
		//'core/paragraph',
  	//'core/image',
  	//'core/gallery',
		//'core/list',
		//'core/quote',
		// 'core/audio',
		//'core/cover-image',
		//'core/file',
		//'core/video',
		//'core/media',

    // Mise en forme
    //'core/table',
    // 'core/verse',
    // 'core/code',
    // 'core/freeform',
    // 'core/html',
    // 'core/preformatted',
    // 'core/pullquote',

    // Mise en page
    //'core/button',
    //'core/columns',
    //'core/media-text',
    // 'core/more',
    // 'core/nextpage',
    // 'core/separator',
    // 'core/spacer',

    // Widgets
    // 'core/shortcode',
    // 'core/archives',
    // 'core/categories',
    // 'core/latest-comments',
    // 'core/latest-posts',

    // Contenus embarqués
    // 'core/embed',
    // 'core-embed/youtube',
    // 'core-embed/facebook',
    // 'core-embed/twitter',
    // 'core-embed/instagram',
    // core-embed/wordpress
    // core-embed/soundcloud
    // core-embed/spotify
    // core-embed/flickr
    // core-embed/vimeo
    // core-embed/animoto
    // core-embed/cloudup
    // core-embed/collegehumor
    // core-embed/dailymotion
    // core-embed/funnyordie
    // core-embed/hulu
    // core-embed/imgur
    // core-embed/issuu
    // core-embed/kickstarter
    // core-embed/meetup-com
    // core-embed/mixcloud
    // core-embed/photobucket
    // core-embed/polldaddy
    // core-embed/reddit
    // core-embed/reverbnation
    // core-embed/screencast
    // core-embed/scribd
    // core-embed/slideshare
    // core-embed/smugmug
    // core-embed/speaker
    // core-embed/ted
    // core-embed/tumblr
    // core-embed/videopress
    // core-embed/wordpress-tv
	);

	return $allowed_blocks;
}
