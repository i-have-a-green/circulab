<?php

function menu_widget_init() {
 
    register_sidebar( array(
        'name' => 'main lang',
        'id' => 'main-lang',
    ) );

    register_sidebar( array(
        'name' => 'mobile lang',
        'id' => 'mobile-lang',
    ) );
}
add_action( 'widgets_init', 'menu_widget_init' );