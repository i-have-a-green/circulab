<?php
/*
* traitement du post du form de Event
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'updateuser',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wpgreen_formUser',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
});

function wpgreen_formUser(){
	$user_id = (int)sanitize_text_field($_POST["user_id"]);
	
	if(empty($_POST["honey_pot"]) && $user_id > 0){

		update_user_meta($user_id, "first_name", sanitize_text_field( $_POST["user_firstname"]));
		update_user_meta($user_id, "last_name", sanitize_text_field( $_POST["user_lastname"]));

		$user_data = wp_update_user( 
			array( 'ID' => $user_id, 
				'user_email' => sanitize_email( $_POST["user_email"] ),
			) 
		);

		if(!empty($_POST['user_password'])){
			wp_set_password($_POST['user_password'], $user_id);
		}

		if(isset($_FILES['user_image'])){

			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/';
			if(!is_dir($uploaddirimg)){
				mkdir($uploaddirimg, 0755);
			}		
			$filename = $uploaddirimg . basename($_FILES['user_image']['name']);
			if (move_uploaded_file($_FILES['user_image']['tmp_name'], $filename)) {
				$wp_filetype = wp_check_filetype($filename, null );
				$attachment = array(
					'guid'           => $wp_upload_dir['url'] . '/'.basename($_FILES['user_image']['name']),
					'post_mime_type' => $wp_filetype['type'],
					//'post_parent' => $product_id,
					'post_title' => preg_replace('/\.[^.]+$/', '', basename($_FILES['user_image']['name'])),
					'post_content' => '',
					'post_status' => 'inherit'
				);
				$attachment_id = wp_insert_attachment( $attachment, $filename);

				$attach_data = wp_generate_attachment_metadata( $attachment_id, $filename );
				wp_update_attachment_metadata( $attachment_id, $attach_data );

				update_field('image', $attachment_id, "user_".$user_id);

			}
		}

		update_field('continent', $_POST["user_continent"], "user_".$user_id);
		update_field('zone', $_POST["user_country"], "user_".$user_id);
		update_field('skills', $_POST["skills"], "user_".$user_id);
		update_field('customer-type', $_POST["user_clients"], "user_".$user_id);
		update_field('job', $_POST["user_job"], "user_".$user_id);
		update_field('paragraph_presentation', $_POST["user_presentation"], "user_".$user_id);

		_e('Your account has been modified', "circulab");
		
	}
}
