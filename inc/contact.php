<?php
/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action( 'wp_ajax_formContact', 'wpgreen_formContact' );
add_action( 'wp_ajax_nopriv_formContact', 'wpgreen_formContact' );
function wpgreen_formContact(){
	if (wp_verify_nonce($_POST['nonceformContact'], 'nonceformContact')) {

		global $wpdb;

		/*$upload_file_text = "";
		if(isset($_FILES['file'])){
			$maintenant = date("d-m-Y_H:i:s");
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/file-form/';
			@mkdir($uploaddirimg, 0755);
			$uploadfile = $uploaddirimg . $maintenant . '-'.basename($_FILES['file']['name']);
			if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                $filename = $upload_dir['baseurl'].'/file-form/' . $maintenant . '-'.basename($_FILES['file']['name']);
                $upload_file_text = "Fichier : ".$filename;
			}
		}*/

		$subject = "Contact depuis le site web - ".sanitize_text_field($_POST['contact_name']);
		$body = '_e("Name", "circulab") : '.sanitize_text_field($_POST['contact_name'])."\r\n";
		$body .= '_e("Company", "circulab") : '.sanitize_text_field($_POST['contact_company'])."\r\n";
		$body .= '_e("Email", "circulab") : '.sanitize_text_field($_POST['contact_email'])."\r\n";
        $body .= '_e("Phone", "circulab") : '.sanitize_text_field($_POST['contact_phone'])."\r\n";
        
        //$body .= $upload_file_text;

        $body .= '_e("Comment", "circulab") : '.sanitize_textarea_field($_POST['contact_comments'])."\r\n";

        $headers[] = 'From: '.get_bloginfo('name').' <'. get_option("admin_email") .'>';
        wp_mail( get_option("admin_email"), $subject, $body, $headers);
        
		$post['post_type']   = 'contact';
		$post['post_status'] = 'publish';
		$post['post_title'] = sanitize_text_field($_POST['contact_name']);
        $post['post_content'] = $body;
        
        $p = wp_insert_post( $post, true );

        /*if(!empty($upload_file_text)){
            contactInsertAttachment($filename, $p);
        }*/

		$body = sprintf(__('Bonjour'."\r\n".'
        L’équipe de '.get_bloginfo('name').' étudie votre demande et vous recontactera dans les meilleurs délais.'."\r\n".'
        Voici un récapitulatif de votre envoi :'."\r\n".'
        %s','arpdl'), $body);

		$headers[] = 'From: '.get_bloginfo('name').' <'. get_option("admin_email") .'>';
		wp_mail( sanitize_text_field($_POST['contact_email']), get_bloginfo('name'), $body, $headers);

		echo 'L’équipe de '.get_bloginfo('name').' étudie votre demande et vous recontactera dans les meilleurs délais.';
	}
	wp_die();
}

function contactInsertAttachment($filename, $p){
	$filetype = wp_check_filetype( basename( $filename ), null );

	// Get the path to the upload directory.
	$wp_upload_dir = wp_upload_dir();

	// Prepare an array of post data for the attachment.
	$attachment = array(
		'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
		'post_mime_type' => $filetype['type'],
		'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
		'post_content'   => '',
		'post_status'    => 'inherit'
	);

	// Insert the attachment.
	$attach_id = wp_insert_attachment( $attachment, $filename, $p );
    update_post_meta( $p, 'attachment', $attach_id );//image à la une
    update_post_meta( $p, 'fileAttachment', $wp_upload_dir['basedir'].'/file-form/'. basename( $filename ) );//image à la une
}