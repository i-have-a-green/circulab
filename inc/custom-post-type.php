<?php
/* joints Custom Post Type Example
This page walks you through creating
a custom post type and taxonomies. You
can edit this one or copy the following code
to create another one.

I put this in a separate file so as to
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

*/

/*
 * Initialise les Custom Post-Types créés
 * ----------------------------------------------------------------------------*/
add_action( 'init', 'initCustomPostTypes' );
function initCustomPostTypes() {

  
  $slug = "event";
  //$slug_rewrite = basename(get_permalink(get_field("archive_event_".weglot_get_current_language(), "options")));
  $args = array(
    'labels'             => defineCPTLabels($slug, __("Event"), __("Events")),
    'description'        => '',
  	'public'             => true,
  	'publicly_queryable' => true,
  	'show_ui'            => true,
  	'show_in_menu'       => true,
  	'query_var'          => true,
  	'has_archive'        => false,
  	'hierarchical'       => false,
  	'show_in_rest'       => false,
    'rest_base'          => false,
  	'menu_position'      => null,
  	'capability_type'    => 'post',
    //'taxonomies'         => array('post_tag'),
  	'supports'           => array('title','thumbnail', 'editor'),
    'menu_icon'          => 'dashicons-tickets',
    //'rewrite'            => array('slug' => $slug_rewrite, 'hierarchical' => false, 'with_front' => false),
  );
  register_post_type($slug, $args);
  add_post_type_support( 'event', 'author' ); 

  // ---------------------------------------------------------------------------


  register_taxonomy(
		'lieu',
    'event',
		array(
			'label' => __( 'Lieu' ),
			'hierarchical' => true,
		)
  );

  $slug = "podcast";
  //$slug_rewrite = basename(get_permalink(get_field("archive_podcast_".weglot_get_current_language(), "options")));
  $args = array(
    'labels'             => defineCPTLabels($slug, "Podcast", "Podcasts"),
    'description'        => '',
  	'public'             => false,
  	'publicly_queryable' => true,
  	'show_ui'            => true,
  	'show_in_menu'       => true,
  	'query_var'          => true,
  	'has_archive'        => true,
  	'hierarchical'       => false,
  	'show_in_rest'       => true,
    'rest_base'          => false,
  	'menu_position'      => null,
  	'capability_type'    => 'post',
    //'taxonomies'         => array('post_tag'),
  	'supports'           => array('title','thumbnail', 'editor', 'excerpt'),
    'menu_icon'          => 'dashicons-media-audio',
    //'rewrite'            => array('slug' => $slug_rewrite, 'hierarchical' => false, 'with_front' => false),
  );
  register_post_type($slug, $args);
 
  /*register_post_type( 'contact', 
		array('labels' 			=> array(
				'name' 				=> __('Contacts', 'ihag'), 
				'singular_name' 	=> __('Contact', 'ihag'), 
			), 
			'menu_position' 	=> 18, 
			'menu_icon' 		=> 'dashicons-email-alt', 
			'hierarchical' 		=> false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => false,
			'supports' 			=> array( 'title', 'editor', 'custom-fields')
	 	) 
	);*/
	register_taxonomy('zone', 'user', array(
		'public'        =>true,
		'single_value' => false,
		'show_admin_column' => true,
		'labels'        =>array(
			'name'                      =>__("Countries"),
			'singular_name'             =>__("Country"),
			'menu_name'                 =>__("Country"),
		),
		'rewrite'       =>false
	));

	register_taxonomy('continent', 'user', array(
		'public'        =>true,
		'single_value' => false,
		'show_admin_column' => true,
		'labels'        =>array(
			'name'                      =>'Continents',
			'singular_name'             =>'Continent',
			'menu_name'                 =>'Continents',
		),
		'rewrite'       =>false
	));

	register_taxonomy('skill', 'user', array(
		'public'        =>true,
		'single_value' => false,
		'show_admin_column' => true,
		'labels'        =>array(
			'name'                      =>__('Skills'),
			'singular_name'             =>__('Skill'),
			'menu_name'                 =>__('Skill'),
		),
		'rewrite'       =>false
	));

	register_taxonomy('business_typology', 'user', array(
		'public'        =>true,
		'single_value' => false,
		'show_admin_column' => true,
		'labels'        =>array(
			'name'                      =>__("Business category"),
			'singular_name'             =>__("Business category"),
			'menu_name'                 =>__("Business category"),
			
		),
		'rewrite'       =>false
	));

	register_taxonomy('certification', array('user'), array(
		'public'        =>true,
		'single_value' => false,
		'show_admin_column' => true,
		'labels'        =>array(
			'name'                      =>'Certification',
			'singular_name'             =>'Certification',
			'menu_name'                 =>'Certifications',
			
		),
		'rewrite'       =>false
	));
    
}


function wpdocs_my_users_menu() {
    
	add_users_page(
		'Certification',//page title
		'Certification',//menu title
		'read',//capability
		'edit-tags.php?taxonomy=certification'
	);
	add_users_page(
		__("business category"),//page title
		__("business category"),//menu title
		'read',//capability
		'edit-tags.php?taxonomy=business_typology'
	);
	add_users_page(
		__("Skills"),//page title
		__("Skills"),//menu title
		'read',//capability
		'edit-tags.php?taxonomy=skill'
	);
	add_users_page(
		__("Country"),//page title
		__("Country"),//menu title
		'read',//capability
		'edit-tags.php?taxonomy=zone'
	);
	add_users_page(
		'Continent',//page title
		'Continent',//menu title
		'read',//capability
		'edit-tags.php?taxonomy=continent'
	);
}
add_action('admin_menu', 'wpdocs_my_users_menu');


/*
 * Ajuste le comportement par défaut des Post-Types par défaut : pages et posts
 * ----------------------------------------------------------------------------*/
add_action( 'init', 'my_add_template_to_posts' );
function my_add_template_to_posts() {
  $post_type_object = get_post_type_object('post');
  $slug_rewrite = basename(get_permalink(get_field("archive_post_".weglot_get_current_language(), "options")));
  $post_type_object->rewrite = array('slug' => $slug_rewrite, 'hierarchical' => false, 'with_front' => false);
}




/*
 * Défini les labels pour le Custom Post-Type en paramètre
 * ----------------------------------------------------------------------------*/
function defineCPTLabels($slug, $name, $names, $feminin = false) {

  if ( $feminin ) :
    $un = "une";
    $nouv = "nouvelle";
    $auc = "aucune";
    $trouv = "trouvée";
    $all = "Toutes";
    $parent = "parente";
  else :
    $un = "un";
    $nouv = "nouveau";
    $auc = "aucun";
    $trouv = "trouvé";
    $all = "Tous";
    $parent = "parent";
  endif;

  $labels = array(
  	'name'               => __($names, 'nbtheme'),
  	'singular_name'      => __($name, 'nbtheme'),
  	'menu_name'          => __($names, 'nbtheme'),
  	'name_admin_bar'     => __($names, 'nbtheme'),
  	'add_new'            => __('Ajouter '.$un.' '.$nouv, 'nbtheme'),
  	'add_new_item'       => __('Ajouter '.$un.' '.strtolower($name), 'nbtheme'),
  	'new_item'           => __(ucfirst($nouv).' '.strtolower($name), 'nbtheme'),
  	'edit_item'          => __('Éditer', 'nbtheme'),
    'update_item'        => __('Mettre à jour', 'nbtheme'),
  	'view_item'          => __('Voir', 'nbtheme'),
  	'all_items'          => __('Tous les '.strtolower($names), 'nbtheme'),
  	'search_items'       => __('Recherche '.$un.' '.strtolower($name), 'nbtheme'),
  	'parent_item'        => __(ucfirst($name).' parent :', 'nbtheme'),
  	'parent_item_colon'  => __(ucfirst($name).' parent :', 'nbtheme'),
  	'not_found'          => __(ucfirst($auc).' '.strtolower($name).' '.$trouv.'.', 'nbtheme'),
  	'not_found_in_trash' => __(ucfirst($auc).' '.strtolower($name).' '.$trouv.' dans la corbeille.', 'nbtheme')
  );

  return $labels;
}






