<?php
/**
 * template name: Page connect
 */
if(!is_user_logged_in()){//si l'utilisateur n'est pas conecté
    wp_redirect(home_url(), 302);
} 

get_header();
while ( have_posts() ) :  the_post();
?>

	<div id="primary">
		<main id="main" class="wrapper">
            <?php the_content();?>
        </main><!-- #main -->
	</div><!-- #primary -->
<?php
endwhile; // End of the loop.
get_footer();
