<?php
/**
 * template name: Page d'archive
 */

get_header();
?>

<div class="wrapper pageTitleContainer"><!-- Page Title -->

	<h1 class="pageTitle"><?php echo get_the_archive_description();?></h1>

</div>

	<div id="primary">
		<main id="archiveContainer" class="wrapper">

		<?php
			$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
			$type = get_query_var("post_type");
			if ( !$type ) :
			    switch ($post->ID):
			      case get_field("archive_event_".weglot_get_current_language(), "options"):
			        $type = "event";
				  break;

				  case get_field("archive_podcast_".weglot_get_current_language(), "options"):
			        $type = "podcast";
			      break;

			      default:
			        $type = "post";
			    endswitch;
			endif;


			// Parametres de query
			$args = array(
			    'posts_per_page' => get_option("posts_per_page"),
			    'paged' => $num_page,
			    'post_type'   => $type,
			    'post_status' => 'publish'
			);
			if($type == "event"){
				$args = array(
					'posts_per_page' => -1,
					'paged' => $num_page,
					'post_type'   => $type,
					'post_status' => 'publish',
					'meta_query' => array(
						array(
						'key'	=> 'date_event',
						'compare'	=> '>=',
						'value'	=> date('Ymd'),
						'type'	=> 'DATETIME'
						),
					),
					'order'	=> 'ASC',
					'orderby'	=> 'meta_value',
					'meta_key'	=> 'date_event',
					'meta_type'	=> 'DATETIME',
				);

				if(isset($_GET['lieu'])){
					$args['tax_query'] = array(
							array(
								'taxonomy' => 'lieu',
								'field' => 'slug',
								'terms' => $_GET['lieu'],
							)
					);
				}

				if($type == "event"){
					$terms = get_terms( array(
						'taxonomy' => 'lieu',
					) );
					echo '<div class="filterContainerEvent filterContainer">';
						echo '<select onchange="location.href=this.options[this.selectedIndex].dataset.slug;">';
						$link = get_permalink( get_field("archive_lieu", "option") ) ;
						echo '<option data-slug="'.$link.'">'.__("All the zones", "circulab").'</option>';
						foreach ( $terms as $term ) {
							$link = add_query_arg( 'lieu', $term->slug, get_permalink( get_field("archive_lieu", "option") ) );
							$select = "";
							if(isset($_GET['lieu']) && $_GET['lieu'] == $term->slug){$select = "selected";}
							echo '<option data-slug="'.$link.'" '.$select.'>' . $term->name . '</option>';
						}
						echo '</select>';
					echo '</div>';
				}
			}
			query_posts($args);
			?>

			<?php
			if ( have_posts() ) :
				echo "<section class='archivePage'>";
				while (have_posts()) : the_post();
					get_template_part('template-parts/archive', $type);
				endwhile;
				echo "</section>";
			?>

			<?php
			if($type == "event"){
				echo '<div class="showMoreContainer">';
				echo '<button class="showMore buttonBlue" id="showMore" onclick="showMoreItem()">'.__("More events", "circulab").'</button>';
				echo '</div>';
			}
			?>

			<?php
			else :
				_e("Aucun contenu", "arpdl");
				$no_results = true;
			endif;
			?>
			<?php
			the_posts_navigation();
			wp_reset_query();
			?>

			<?php the_content();?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
