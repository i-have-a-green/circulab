<?php
/**
 * template name: Page User
 */
if(!is_user_logged_in()){//si l'utilisateur n'est pas conecté
    wp_redirect(home_url(), 302);
} 
get_header();
while ( have_posts() ) :  the_post();

	the_content();

	$user_id = get_current_user_id();
	$user = wp_get_current_user();
	$user_meta = get_user_meta( $user_id );
	?>

	<div id="primary">
		<main id="main" class="wrapper">
            <form id="form-user" name="form-user" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data" class="bloc">
				<input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id;?>">
				<input type="hidden" name="honey_pot" id="honey_pot" value="">
				<div class="contResponse" id="contResponse"></div>
					<div class="formCont">
						<label for="user_firstname"><?php _e("First name", "circulab");?></label>
						<input type="text" name="user_firstname" id="user_firstname" value="<?php echo get_user_meta($user_id, "first_name", true);?>">
					</div>

					<div class="formCont">
						<label for="user_lastname"><?php _e("Last name", "circulab");?></label>
						<input type="text" name="user_lastname" id="user_lastname" value="<?php echo get_user_meta( $user_id, 'last_name', true );?>">
					</div>		
					
					<div class="formCont">
						<label for="user_email"><?php _e("Email", "circulab");?></label>
						<input type="text" name="user_email" id="user_email" value="<?php echo $user->user_email;?>">
					</div>	

					<div class="formCont">
						<label for="user_password"><?php _e("Password", "circulab");?></label>
						<input type="password" minlength="8" name="user_password" id="user_password" value="" placeholder="laissez vide si pas de changement">
					</div>
					
					<div class="formCont">
						<label for="user_image"><?php _e("Picture", "circulab");?></label>
						<input type="file" name="user_image" id="user_image">
						<?php
							if(get_field("image", "user_".$user_id)){
								$image = get_field("image", "user_".$user_id);
								$size = 'thumbnail';
								if( $image ) { 
									echo '<div style="max-width:40px;height:auto">';
									echo wp_get_attachment_image( $image, $size );
									echo '</div>';
								}
							}
							else{
								echo '<div style="max-width:40px;height:auto;">';
								echo get_avatar( $user_id, 32 );
								echo '</div>';
							}
						?>
					</div>

					<div class="formCont formRadio">
						<p>
							<label for="user_continent"><?php _e("Continent", "circulab");?></label>

							<?php $user_continent = get_field("continent", "user_".$user_id);?>

							<select name="user_continent" id="user_continent">
								<?php
									$continents = get_terms(
										array(
											"taxonomy" => "continent",
											"hide_empty" => false
										)
									);

									foreach($continents as $continent):?>
										<option value="<?php echo $continent->term_id;?>" <?php selected( $user_continent[0],$continent->term_id);?>>
											<?php echo $continent->name;?>
										</option>
									<?php endforeach;
								?>
							</select>
						</p>

					</div>

					<div class="formCont formRadio">
						<p>
							<label for="user_country"><?php _e("Country", "circulab");?></label>

							<?php $user_country = get_field("zone", "user_".$user_id); ?>

							<select name="user_country" id="user_country">
								<?php
									$countries = get_terms(
										array(
											"taxonomy" => "zone",
											"hide_empty" => false
										)
									);

									foreach($countries as $country):?>
										<option value="<?php echo $country->term_id;?>" <?php selected( $user_country[0],$country->term_id);?>>
											<?php echo $country->name;?>
										</option>
									<?php endforeach;
								?>
							</select>
						</p>

					</div>

					<div class="formCont">
						<label for="user_area"><?php _e("City", "circulab");?></label>
						<input type="text" name="user_area" id="user_area" value="<?php echo get_field( 'area', 'user_'.$user_id );?>">
					</div>	

					<div class="formCont">
						<label for="user_phone"><?php _e("Phone", "circulab");?></label>
						<input type="text" name="user_phone" id="user_phone" value="<?php echo get_field( 'phone', 'user_'.$user_id );?>">
					</div>	

					<div class="formCont formRadio">
						<p>
							<b><?php _e("Skills", "circulab");?> : </b><br>

							<?php $user_skills = get_field("skills", "user_".$user_id); ?>

								<?php
									$skills = get_terms(
										array(
											"taxonomy" => "skill",
											"hide_empty" => false
										)
									);

									foreach($skills as $skill):?>
										<input type="checkbox" style="width:auto;" name="skills[]" value="<?php echo $skill->term_id;?>" <?php echo (in_array( $skill->term_id, $user_skills)) ? 'checked' : '' ;?>>
										<label><?php echo $skill->name;?></label><br>
									<?php endforeach;
								?>
							</select>
						</p>

					</div>

					<div class="formCont">
						<label for="user_clients"><?php _e("Latest clients", "circulab");?></label>
						<input type="text" name="user_clients" id="user_clients" value="<?php echo get_field( 'customer-type', 'user_'.$user_id );?>">
					</div>	

					<div class="formCont">
						<label for="user_job"><?php _e("Job", "circulab");?></label>
						<input type="text" name="user_job" id="user_job" value="<?php echo get_field( 'job', 'user_'.$user_id );?>">
					</div>
					
					<div class="formCont formRadio" style="text-align:left;">
						<label for="user_presentation"><?php _e("Description", "circulab");?></label>
						<p>
							<textarea rows="6" name="user_presentation" id="user_presentation"><?php echo get_field( 'paragraph_presentation', 'user_'.$user_id );?></textarea>
						</p>
					</div>

					<div class="formButton">
						<input class="button buttonBlue" type="submit" id="user_send" value="<?php _e("Send","circulab");?>">
					</div>			
				
            </form>
            
        </main><!-- #main -->
	</div><!-- #primary -->
<?php
endwhile; // End of the loop.
get_footer();
