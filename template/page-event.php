<?php
/**
 * template name: Page Event
 */
if(!is_user_logged_in()){//si l'utilisateur n'est pas conecté
    wp_redirect(home_url(), 302);
} 
get_header();
while ( have_posts() ) :  the_post();
?>

	<div id="primary">
		<main id="main" class="wrapper">
            <form id="form-event" name="form-event" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data" class="bloc">
               
				<div class="contResponse" id="contResponse"></div>

				<div class="formColumnContainer">

					<div class="formColumnLeft">

						<div class="formCont">
							<label for="title"><?php _e("Event title","circulab");?>*</label>
							<input type="text" value="" name="title" id="title" placeholder="<?php _e("Event title","circulab");?>*" required >
								
						</div>

						<div class="formCont">
							<label for="date_event"><?php _e("Date","circulab");?>*</label>
							<input type="date" value="" name="date_event" id="date_event" required>
								
						</div>

						<div class="formCont">
							<label for="date_event_end"><?php _e("End date","circulab");?></label>
							<input type="date" value="" name="date_event_end" id="date_event_end" >
						</div>

						<div class="formCont">
							<label for="date_texte"><?php _e("Text date","circulab");?>*</label>
							<input type="text" value="" name="date_texte" id="date_texte" placeholder="<?php _e("Text date","circulab");?>*" required >
						</div>

					</div><!--.formColumnLeft-->

					<div class="formColumnRight">

						<div class="formCont formRadio">
							<?php _e("City : ", "circulab");?>*<br>
							<p>
							
							<?php
								$terms = get_terms( array(
									'taxonomy' => 'lieu',
								) );
								foreach ( $terms as $term ) :?>
									<input type="radio" name="lieu" id="lieu_<?php echo $term->term_id;?>" value="<?php echo $term->term_id;?>" > 
									<label for="lieu_<?php echo $term->term_id;?>" ><?php echo $term->name;?></label><br>
								<?php endforeach;?>
								<input type="radio" name="lieu" id="add_city_0" value="0" required> 
								<label for="add_city_0" ><?php _e("Add a new city","circulab");?></label><br>
							</p>
						</div>
						<div id="newCity">
							<div class="formCont">
									<input type="text" value="" name="city" id="city" placeholder="<?php _e("New city","circulab");?>" />
									<label for="city"><?php _e("New city","circulab");?></label>
							</div>
							<div class="formCont">
							<p><label for="file"><?php _e("City picture : ","circulab");?></label>
								<input type="file"  name="file" id="file"  /></p>
									
							</div>
						</div>
						<div class="formCont">
								<input type="url" value="" name="link" id="link" placeholder="<?php _e("URL to register","circulab");?>" />
								<label for="link"><?php _e("URL to register","circulab");?></label>
						</div>

						<div class="formCheckBox">
								<input type="checkbox" name="check" id="event_check" required />
								<label for="event_check">
										<?php _e("By checking this box and submitting this form, I agree that my personal data may be used to contact me in connection with my request indicated in this form. (No other processing will be carried out with your information).","circulab");?>
								</label>
						</div>

					</div><!--.formColumnRight-->

				</div><!--.formColumnContainer-->

                <div class="formButton">
                    <input class="button buttonBlue" type="submit" id="event_send" value="<?php _e("Send","circulab");?>">
                </div>
            </form>
            <?php the_content();?>
        </main><!-- #main -->
	</div><!-- #primary -->
<?php
endwhile; // End of the loop.
get_footer();
