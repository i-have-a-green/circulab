<?php
/**
 * template name: Page community
 */

get_header();
while ( have_posts() ) :  the_post();
?>

<div class="wrapper pageTitleContainer"><!-- Page Title -->

	<h1 class="pageTitle"><?php echo get_the_archive_description();?></h1>

</div>


	<div id="primary" >
        <div class="wrapper">
        <?php the_content();?>
        </div>
        <main id="main" class="wrapper commmunityContainer <?php if(is_user_logged_in()):?>isLoggedIn<?php endif;?>">

		<div class="bloc sidebar">
            <div id="map-community" class="mapContainer">
                <?php if(is_user_logged_in()):?>
                    <?php /* if( isset($_GET["zone"]) && !empty($_GET["zone"]) && $_GET["zone"] == "Asia-Oceania" ):?>
                        <img src="<?php echo get_stylesheet_directory_uri();?>/image/map-asia-oceania.png">
                    <?php elseif( isset($_GET["zone"]) && !empty($_GET["zone"]) && $_GET["zone"] == "America" ):?>
                        <img src="<?php echo get_stylesheet_directory_uri();?>/image/map-america.png">
                    <?php elseif( isset($_GET["zone"]) && !empty($_GET["zone"]) && $_GET["zone"] == "Africa-Europe" ):?>
                        <img src="<?php echo get_stylesheet_directory_uri();?>/image/map-africa-europa.png">
                    <?php else:?>
                        <img src="<?php echo get_stylesheet_directory_uri();?>/image/map.png">
                    <?php endif; */?>
                <?php 
                else: 
                    the_post_thumbnail("large");
                endif;
                ?>
            </div>

            <?php switch_to_blog(1);?>

            <form  class="filterContainer" method="GET" action="<?php the_permalink($post->ID);?>" id="form-community">
                <h2><?php _e("Look for a Circulab community member", "circulab");?></h2>
                <div class="filterItem">
                    <label label="form-community-continent"><?php _e("Zone ","circulab");?></label>
                    <select name="continent" id="form-community-continent" class="hasBlueBg">
                        <option value=""><?php _e("Zone ","circulab");?></option>
                        <?php 
                            $terms = get_terms( array( 'taxonomy' => 'continent','hide_empty' => false, ) );
                            $continent_par_default = get_field("continent_par_default");
                            foreach($terms as $term):?>
                            <option value="<?php echo $term->term_id;?>" 
                                <?php 
                                    if(isset($_GET['continent'])) {
                                        selected( $term->term_id, $_GET['continent'] );
                                    }else if(!empty($continent_par_default[0])){
                                        selected( $term->term_id, $continent_par_default[0] );
                                    } 
                                    ?>>
                                <?php echo $term->name;?>
                            </option> 
                            <?php endforeach;
                        ?>
                    </select>
                </div>
                
                <div class="filterItem">
                    <label label="form-community-zone"><?php _e("Zone","circulab");?></label>
                    <select name="zone" id="form-community-zone" class="hasBlueBg">
                        <option value=""><?php _e("Country","circulab");?></option>
                        <?php $terms = get_terms( array( 'taxonomy' => 'zone','hide_empty' => false, ) );
                            foreach($terms as $term):?>
                            <option value="<?php echo $term->term_id;?>" <?php if(isset($_GET['zone'])) {selected( $term->term_id, $_GET['zone'] );} ?>><?php echo $term->name;?></option> 
                            <?php endforeach;
                        ?>
                    </select>
                </div>

                <div class="filterItem">
                <label label="form-community-skill"><?php _e("Skill","circulab");?></label>
                <select name="skill" id="form-community-skill" class="hasLightBlueBg">
                    <option value=""><?php _e("Skill","circulab");?></option>
                    <?php $terms = get_terms( array( 'taxonomy' => 'skill','hide_empty' => false, ) );
                        foreach($terms as $term):?>
                        <option value="<?php echo $term->term_id;?>" <?php if(isset($_GET['skill'])) {selected( $term->term_id, $_GET['skill'] );} ?>><?php echo $term->name;?></option> 
                        <?php endforeach;
                    ?>
                </select>
                </div>

                <div class="filterItem">
                    <label label="form-community-type-of-company"><?php _e("Type of client","circulab");?></label>
                    <select name="type-of-company" id="form-community-type-of-company" class="hasGreenBg">
                        <option value=""><?php _e("Type of client","circulab");?></option>
                        <?php $terms = get_terms( array( 'taxonomy' => 'business_typology','hide_empty' => false, ) );
                            foreach($terms as $term):?>
                            <option value="<?php echo $term->term_id;?>" <?php if(isset($_GET['type-of-company'])) {selected( $term->term_id, $_GET['type-of-company'] );} ?>><?php echo $term->name;?></option> 
                            <?php endforeach;
                        ?>
                    </select>
                </div>

                <div class="filterItem">
                    <label label="form-community-certification"><?php _e("Certification","circulab");?></label>
                    <select name="certification" id="form-community-certification" class="hasLightGreenBg">
                        <option value=""><?php _e("Certification","circulab");?></option>
                        <?php $terms = get_terms( array( 'taxonomy' => 'certification','hide_empty' => false, ) );
                            foreach($terms as $term):?>
                            <option value="<?php echo $term->term_id;?>" <?php if(isset($_GET['certification'])) {selected( $term->term_id, $_GET['certification'] );} ?>><?php echo $term->name;?></option> 
                            <?php endforeach;
                        ?>
                    </select>
                </div>

                <?php //if(is_user_logged_in()):?>
                    <!--
                    <div class="filterItem">
                        <label label="form-community-mastermind"><?php _e("Mastermind organization","circulab");?></label>
                        <select name="mastermind" id="form-community-mastermind" class="hasRedBg">
                            <option value=""><?php _e("Mastermind organization","circulab");?></option>
                            <option value="1"<?php if(isset($_GET['mastermind'])) {selected( 1, $_GET['mastermind'] );} ?>><?php _e("Yes","circulab");?></option>
                            <option value="0"<?php if(isset($_GET['mastermind'])) {selected( 1, $_GET['mastermind'] );} ?>><?php _e("No","circulab");?></option>
                        </select>
                    </div>

                    <div class="filterItem">
                        <label label="form-community-meetup"><?php _e("Meetup organization","circulab");?></label>
                        <select name="meetup" id="form-community-meetup" class="hasPurpleBg">
                            <option value=""><?php _e("Meetup organization","circulab");?></option>
                            <option value="1" <?php if(isset($_GET['meetup'])) {selected( 1, $_GET['meetup'] );} ?>><?php _e("Yes","circulab");?></option>
                            <option value="0" <?php if(isset($_GET['meetup'])) {selected( 1, $_GET['meetup'] );} ?>><?php _e("No","circulab");?></option>
                        </select>
                    </div>

                    <div class="filterItem">
                        <label label="form-community-customer"><?php _e("Circulab client","circulab");?></label>
                        <select name="customer" id="form-community-customer" class="hasOrangeBg">
                            <option value=""><?php _e("Circulab client","circulab");?></option>
                            <option value="1" <?php if(isset($_GET['customer'])) {selected( 1, $_GET['customer'] );} ?>><?php _e("Yes","circulab");?></option>
                            <option value="0" <?php if(isset($_GET['customer'])) {selected( 1, $_GET['customer'] );} ?>><?php _e("No","circulab");?></option>
                        </select>
                    </div>
                <?php// endif;?>
                    -->
            </form>

            

		</div><!-- /.sidebar -->
        <div id="community-users" class="blocUserContainer <?php if(is_user_logged_in()):?>isLoggedIn<?php endif;?>">
			<?php
  
			$args = array (
                'role'       => 'circulab',
                'order'      => 'ASC',
                'orderby'    => 'display_name',
                'fields'     => array('ID'),
            );
           /*$meta_query = array('relation' => 'AND');
            if(is_user_logged_in()):
                if( isset($_GET["mastermind"]) && $_GET["mastermind"] == "1" ){
                    $meta_query[] = array(
                        'key' => 'mastermind',
                        'compare' => '=',
                        'value'   => sanitize_text_field( $_GET["mastermind"] )
                    );
                }
                else if( isset($_GET["mastermind"]) && $_GET["mastermind"] == "0" ){
                    $meta_query[] = array(
                        'key' => 'mastermind',
                        'compare' => '=',
                        'value'   => sanitize_text_field( $_GET["mastermind"] )
                    );
                }

                if( isset($_GET["meetup"]) && $_GET["meetup"] == "1" ){
                    $meta_query[] = array(
                        'key' => 'meetup',
                        'compare' => '=',
                        'value'   => sanitize_text_field( $_GET["meetup"] )
                    );
                }
                else if( isset($_GET["meetup"]) && $_GET["meetup"] == "0" ){
                    $meta_query[] = array(
                        'key' => 'meetup',
                        'compare' => '=',
                        'value'   => sanitize_text_field( $_GET["meetup"] )
                    );
                }

                if( isset($_GET["customer"]) && $_GET["customer"] == "1" ){
                    $meta_query[] = array(
                        'key' => 'customer',
                        'compare' => '=',
                        'value'   => sanitize_text_field( $_GET["customer"] )
                    );
                }
                else if( isset($_GET["customer"]) && $_GET["customer"] == "0" ){
                    $meta_query[] = array(
                        'key' => 'customer',
                        'compare' => '=',
                        'value'   => sanitize_text_field( $_GET["customer"] )
                    );
                }

            endif;

            $args['meta_query'] = $meta_query;*/
            $wp_user_query = new WP_User_Query( $args );
            $users = $wp_user_query->get_results();
            $pasUser = 9;
            $display_users = array();
			$i = 0;
            // Check for results
            if ( ! empty( $users ) ) :
                // loop through each author
                foreach ( $users as $user ) :
                    // get all the user's data
                    $show = true;
                    if( isset($_GET["continent"]) && !empty($_GET["continent"]) ){
                        $show = false;
                        if($continents = get_field("continent", "user_".$user->ID)){
                            foreach($continents as $continent){
                                if($continent == sanitize_text_field( $_GET["continent"] )){
                                    $show = true;
                                }
                            }
                        }
                    }
                    else if(!isset($_GET["continent"]) && !empty($continent_par_default[0])){
                        $show = false;
                        if($continents = get_field("continent", "user_".$user->ID)){
                            foreach($continents as $continent){
                                if($continent == $continent_par_default[0] ){
                                    $show = true;
                                }
                            }
                        }
                    }

                    if( isset($_GET["skill"]) && !empty($_GET["skill"]) ){
                        $show = false;
                        if($skills = get_field("skills", "user_".$user->ID)){
                            foreach($skills as $skill){
                                if($skill == sanitize_text_field( $_GET["skill"] )){
                                    $show = true;
                                }
                            }
                        }
                    }

                    
                    if($show && isset($_GET["type-of-company"]) && !empty($_GET["type-of-company"]) ){
                        $show = false;
                        if($business_typologys = get_field("business_typolopy", "user_".$user->ID)){
                            foreach($business_typologys as $business_typology){
                                if($business_typology == sanitize_text_field( $_GET["type-of-company"] )){
                                    $show = true;
                                }
                            }
                        }
                    }

                    if($show && isset($_GET["zone"]) && !empty($_GET["zone"]) ){
                        $show = false;
                        if($zones = get_field("zone", "user_".$user->ID)){
                            foreach($zones as $zone){
                                if($zone == sanitize_text_field( $_GET["zone"] )){
                                    $show = true;
                                }
                            }
                        }
                    }

                    if($show && isset($_GET["certification"]) && !empty($_GET["certification"]) ){
                        $show = false;
                        if($zones = get_field("certification", "user_".$user->ID)){
                            foreach($zones as $zone){
                                if($zone == sanitize_text_field( $_GET["certification"] )){
                                    $show = true;
                                }
                            }
                        }
                    }

                    if($show):
                        if($i < $pasUser):
                            set_query_var( 'user_id', absint( $user->ID ) );
                            get_template_part('template-parts/archive', "community");
                            $i++;
                        endif;
                        $display_users[] = $user->ID;
                    endif;

                    

                endforeach;
                
                echo '<script>
                    var display_users = '.json_encode($display_users).';
                    nb_users_display = '.$i.';
                    pasUser = '.$pasUser.';
                    </script>';
            
            else :
                _e("Aucun contenu", "arpdl");
                $no_results = true;
            endif;
            ?>

            <?php restore_current_blog();?>
		</div><!-- .blocUserContainer -->
        
        <?php if($i < sizeof($display_users)):?>
            <!-- to load more users -->
            <div class="toLoad">
                <button class="button buttonPink" id="loadMoreUsers">
                    <?php _e('See more members', 'circulab') ;?>
                <buuton>
            </div>
        <?php endif;?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
endwhile; // End of the loop.
get_footer();
