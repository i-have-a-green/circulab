<?php
/**
 * template name: Page Contact
 */

get_header();
while ( have_posts() ) :  the_post();
?>

	<div id="primary">
		<main id="main" class="wrapper">
            <form id="form-contact" name="form-contact" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data" class="bloc">
                <input type="hidden" name="action" value="formContact">
                <?php wp_nonce_field('nonceformContact', 'nonceformContact'); ?>


								<div class="contResponse" id="contResponse"></div>

								<div class="formColumnContainer">

									<div class="formColumnLeft">

										<div class="formCont">
												<input type="text" value="" name="contact_name" id="contact_name" placeholder="<?php _e("Name","circulab");?>*" required />
												<label for="contact_name"><?php _e("Name","circulab");?>*</label>
										</div>

										<div class="formCont">
												<input type="tel" value="" name="contact_phone" id="contact_phone" placeholder="<?php _e("Phone","circulab");?>" />
												<label for="contact_phone"><?php _e("Phone","circulab");?></label>
										</div>

										<div class="formCont">
												<input type="email" value="" name="contact_email" id="contact_email" placeholder="<?php _e("Email","circulab");?>*" required />
												<label for="contact_email"><?php _e("Email","circulab");?>*</label>
										</div>

										<div class="formCont">
												<input type="text" value="" name="contact_company" id="contact_company" placeholder="<?php _e("Entreprise","circulab");?>"  />
												<label for="contact_company"><?php _e("Entreprise","circulab");?></label>
										</div>

										<div class="formCont">
												<input type="text" value="" name="contact_country" id="contact_country" placeholder="<?php _e("Country","circulab");?>"  />
												<label for="contact_country"><?php _e("Country","circulab");?></label>
										</div>

									</div><!--.formColumnLeft-->

									<div class="formColumnRight">

										<div class="formCont">
												<textarea name="contact_comments"  id="contact_comments" placeholder="<?php _e("Comments", "circulab");?>*" required ></textarea>
												<label for="contact_comments"><?php _e("Comments", "circulab");?>*</label>
										</div>

										<div class="formCheckBox">
												<input type="checkbox" name="check" id="contact_check" required />
												<label for="contact_check">
														<?php _e("By checking this box and submitting this form, I agree that my personal data may be used to contact me in connection with my request indicated in this form. (No other processing will be carried out with your information).","circulab");?>
												</label>
										</div>

									</div><!--.formColumnRight-->

								</div><!--.formColumnContainer-->

                <div class="formButton">
                    <button class="button buttonBlue" type="submit" id="contact_send"><?php _e("Send","circulab");?></button>
                </div>
            </form>
            <?php the_content();?>
        </main><!-- #main -->
	</div><!-- #primary -->
<?php
endwhile; // End of the loop.
get_footer();
