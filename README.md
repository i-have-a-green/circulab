# Circulab v2

Prérequis :

- Node.js (https://nodejs.org/)
- npm (https://www.npmjs.com/)
- Wordpress (http://wordpress.org/latest.tar.gz)
- Thème Parent Susty (https://github.com/jacklenox/susty/archive/master.zip)

1 - Installer Wordpress

2 - Installer les pluggins suivants :
```console
ACF PRO
Polylang
```
3 - Installer et activer le thèmse Susty (via l'admin Wordpress)

4 - Ajouter le thème Circulab v2 à l'emplacement suivant :
```console
[projet] / wp-content / themes / circulab-v2
```

5 - Installer les dépendances avec npm
```console
$ npm install
```

6 - Lancer le script
```console
$ npm run watch
```

7 - Lancer BrowserSync [optionnel]
```console
$ npm run start
```

-----------

# Notes complémentaires :

Retirer les thèmes Wordpress inutiles (twentytwenty, twentynineteen, ect …)
```console
$ cd wp-content/themes/
$ rm -rf twenty
```

Modifier le chemin des scripts en fonction de l'environnement local
Chemin par défaut : 
```console
'localhost/circulab/'
```
dans package.json (ligne 44)

Pluggins Wordpress utilisés :
- ACF PRO - b3JkZXJfaWQ9OTE1Mzh8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTEwLTEyIDE0OjMyOjI3
- Yoast SEO
- Polylang