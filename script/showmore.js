// ouverture - fermeture du menu mobile - source @ https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_toggle_class_crossbrowser

// Classlist #menuConnect
function showMoreItem() {
  var element = document.getElementById("archiveContainer");

  if (element.classList) {
    element.classList.toggle("showMoreItems");
  } else {
    var classes = element.className.split(" ");
    var i = classes.indexOf("showMoreItems");

    if (i >= 0)
      classes.splice(i, 1);
    else
      classes.push("showMoreItems");
      element.className = classes.join(" ");
  }
}
