document.addEventListener('DOMContentLoaded', function() {
    if (document.getElementById('form-community')) {

        document.getElementById('form-community-zone').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-continent').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-skill').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-type-of-company').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-certification').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

       /* document.getElementById('form-community-mastermind').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-meetup').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });

        document.getElementById('form-community-customer').addEventListener('change', function(e) {
            document.getElementById('form-community').submit();
        });*/

        



        var els = document.getElementsByClassName("blocSingleUSer");
        Array.prototype.forEach.call(els, function(el) {
            el.addEventListener("click", function(e) {
                //e.preventDefault();
                modalID = document.getElementById(el.dataset.modal);
                modalID.classList.add("active");
                document.getElementById("BGmodal").classList.add("active");
            });
        });

        var closeEls = document.getElementsByClassName("close-modal");
        Array.prototype.forEach.call(closeEls, function(el) {
            el.addEventListener("click", function(e) {
                e.preventDefault();
                modal = document.getElementById(el.dataset.modal);
                if (modal.classList.contains("active")) modal.classList.remove("active");
                document.getElementById("BGmodal").classList.remove("active");
            });
        });

    }

    if (document.getElementById('loadMoreUsers')) {
        el = document.getElementById('loadMoreUsers');
        el.addEventListener("click", function() {
            var comUser = document.getElementById('community-users');
            var offsetBottom = comUser.offsetTop + comUser.offsetHeight;
            var tab = [];
            for(var i = nb_users_display; i < (pasUser + nb_users_display); i++){
                if(display_users[i]){
                    tab.push(display_users[i]); 
                }
            }
            nb_users_display = nb_users_display + pasUser;
            var formData = new FormData();
            formData.append("display_users", JSON.stringify(tab));
            xhr = new XMLHttpRequest();
            xhr.open('POST', resturl+'display_users' , true);
            xhr.onload = function() {
                if (xhr.status === 200) {
                    //console.log(xhr.response);
                    document.getElementById('community-users').insertAdjacentHTML('beforeend', xhr.response);
                    var els = document.getElementsByClassName("blocSingleUSer");
                    Array.prototype.forEach.call(els, function(el) {
                        el.addEventListener("click", function(e) {
                            //e.preventDefault();
                            modalID = document.getElementById(el.dataset.modal);
                            modalID.classList.add("active");
                            document.getElementById("BGmodal").classList.add("active");
                        });
                    });
                    var closeEls = document.getElementsByClassName("close-modal");
                    Array.prototype.forEach.call(closeEls, function(el) {
                        el.addEventListener("click", function(e) {
                            e.preventDefault();
                            modal = document.getElementById(el.dataset.modal);
                            if (modal.classList.contains("active")) modal.classList.remove("active");
                            document.getElementById("BGmodal").classList.remove("active");
                        });
                    });

                    window.scrollTo(0, (offsetBottom - 80));

                }
            };
            xhr.send(formData); 

        });
    }
});