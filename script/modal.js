

document.addEventListener("DOMContentLoaded", function(event) {
    
    //ouverture de la modal
    var els = document.getElementsByClassName("buttonModal");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) { 
            e.preventDefault(); 
            modal = document.getElementById(el.dataset.idModal);
            modal.classList.add("active");
            document.getElementById("BGmodal").classList.add("active");
        });
    });

    //fermeture de la modal 
    var closeModals = document.getElementsByClassName("closeModal");
    Array.prototype.forEach.call(closeModals, function(closeModal) {
        closeModal.addEventListener("click", function(e) { 
            e.preventDefault();

            var modals = document.getElementsByClassName("modal");
            Array.prototype.forEach.call(modals, function(modal) {
                modal.classList.remove("active");
                document.getElementById("BGmodal").classList.remove("active");
            });
        });
    });
    
}); 