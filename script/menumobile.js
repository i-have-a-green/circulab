// ouverture - fermeture du menu mobile - source @ https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_toggle_class_crossbrowser

// Classlist #menuMobile
function toggleMenuMobile() {
  var element = document.getElementById("menuMobile");
  var topbar = document.getElementById("menuTopbarLogo");

  if (element.classList) {
    element.classList.toggle("menuMobileOpen");
    topbar.classList.toggle("menuMobileOpen");
  } else {
    var classes = element.className.split(" ");
    var i = classes.indexOf("menuMobileOpen");

    if (i >= 0)
      classes.splice(i, 1);
    else
      classes.push("menuMobileOpen");
      element.className = classes.join(" ");
  }
}
