window.addEventListener("scroll", myFunction);

function myFunction() {
    if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
        document.getElementById("masthead").className = "hasShadow";
    } else {
        document.getElementById("masthead").className = "";
    }
}
