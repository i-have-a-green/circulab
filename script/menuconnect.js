// ouverture - fermeture du menu mobile - source @ https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_toggle_class_crossbrowser

// Classlist #menuConnect
function toggleMenuConnect() {
  var element = document.getElementById("menuConnect");

  if (element.classList) {
    element.classList.toggle("menuConnectOpen");
  } else {
    var classes = element.className.split(" ");
    var i = classes.indexOf("menuConnectOpen");

    if (i >= 0)
      classes.splice(i, 1);
    else
      classes.push("menuConnectOpen");
      element.className = classes.join(" ");
  }
}
