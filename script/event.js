document.addEventListener('DOMContentLoaded', function () {
    if (document.getElementById('form-event')) {
        document.getElementById('form-event').addEventListener('submit', function (e) {
            e.preventDefault();
            document.getElementById('event_send').disabled = true;
            var form = document.forms.namedItem("form-event");
            var formData = new FormData(form);
            xhr = new XMLHttpRequest();
            xhr.open('POST', resturl+'addevent', true);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    document.getElementById('event_send').disabled = false;
                    document.getElementById("contResponse").innerHTML = xhr.response;
                    window.scrollTo({
                        top: document.getElementById("content").offsetTop,
                        behavior: 'smooth',
                    });
                    document.getElementById("form-event").classList.add("response"); 
                }
            }; 
            xhr.send(formData);
        });


        var radios = document.querySelectorAll('input[type=radio][name="lieu"]');
        Array.prototype.forEach.call(radios, function(radio) {
         radio.addEventListener('change', function(){
             if(this.value == 0){
                 document.getElementById("newCity").style.display = "block";
                 document.getElementById("city").attributes.required = "required";
                 document.getElementById("file").attributes.required = "required";
             }
             else{
                document.getElementById("newCity").style.display = "none";
                document.getElementById("city").attributes.required = "";
                document.getElementById("file").attributes.required = "";
             }
         });
        });

    }
});