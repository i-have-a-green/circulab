document.addEventListener('DOMContentLoaded', function () {
    if (document.getElementById('form-user')) {
        document.getElementById('form-user').addEventListener('submit', function (e) {
            e.preventDefault();
            document.getElementById('user_send').disabled = true;
            var form = document.forms.namedItem("form-user");
            var formData = new FormData(form);
            xhr = new XMLHttpRequest();
            xhr.open('POST', resturl+'updateuser', true);
            xhr.onload = function () {
                if (xhr.status === 200) {
                    document.getElementById('user_send').disabled = false;
                    document.getElementById("contResponse").innerHTML = xhr.response;
                    window.scrollTo({
                        top: document.getElementById("content").offsetTop,
                        behavior: 'smooth',
                    });
                    document.getElementById("form-user").classList.add("response"); 
                }
            }; 
            xhr.send(formData);
        });
    }
}); 