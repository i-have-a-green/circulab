

	</div><!-- div #content -->


			<footer id="footer">


				<!-- Safe space on desktop // because of the graphic elements in background  -->
				<div class="footerSafeSpace">

					<div class="footerHeader">

						<?php
							echo ihag_menu('footer');
						?>

						<div class="footerSocialMedia">

							<?php

								while ( have_rows('menu_reseaux_sociaux_footer', 'option') ) : the_row();
									?>
									<a href="<?php the_sub_field("link");?>" class="socialMediaItem" target="_blank">
										<img src="<?php the_sub_field("svg");?>">
									</a>
									<?php
								endwhile;

							?>

						</div>

					</div>

					<!-- Line of text -->
					<div class="footerLastText fontTitle">

						<?php
							the_field("footer_ligne", "option");
						?>

					</div>

				</div><!-- div .footerSafeSpace -->

			</footer>

			<!-- Graphic elements in background -->
			<div class="footerBackgroundTrick isHidenOnMobile"></div>
			<!--  div.footerBlueBackground-->

</div><!-- div #page -->

<?php wp_footer(); ?>
<script><?php the_field("script-footer", "option");?></script>
</body>
</html>
