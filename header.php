<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Susty
 */

if(isset($_GET["deconnectUser"])){
	wp_logout();
	wp_redirect(home_url(), 302);
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!-- Google Font Link : Montserrat Font  -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700&display=swap&subset=latin-ext" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="icon" href="<?php echo the_field("favicon", "option"); ?>" sizes="32x32">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >


<?php
	if(isset($_GET["redirectLang"])){
		setcookie("redirectLang", "1", time() + (3600 * 24 * 7 ), '/', str_replace(array('us.', 'es.', 'ch.'),'',$_SERVER['HTTP_HOST']));
	}
	elseif(!isset($_COOKIE['redirectLang'])){
		$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5);
		if($lang == 'en-US'){
			if(substr($_SERVER['HTTP_HOST'], 0, 2) != "us"){
				wp_redirect(get_field('url-en-us', 'option')."?redirectLang");
			}
		} 
		else{
			$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
			if($lang == "fr"){
				wp_redirect(get_field('url-fr', 'option')."?redirectLang");
			} 
			else if($lang == "es"){
				if(substr($_SERVER['HTTP_HOST'], 0, 2) != "es"){
					wp_redirect(get_field('url-es', 'option')."?redirectLang");
				}
			} 
			else if($lang == "ch"){
				if(substr($_SERVER['HTTP_HOST'], 0, 2) != "ch"){
					wp_redirect(get_field('url-ch', 'option')."?redirectLang");
				}
			}
			else{
				wp_redirect(get_field('url-en', 'option')."?redirectLang");
			}
	
		}
	}
	

	

?>
<div id="BGmodal"></div>
<?php if(!is_user_logged_in()): //si l'utilisateur n'est pas conecté
	

?>

<div id="connectModal" class="modal">
	<button class="closeModal buttonGhost">X</button>
	<?php
	//logo champs ACF la taille "logo" est gérée depuis le fichier inc/image.php
	$image = get_field('logoCommunity','option');
	$size = 'logoCommunity';
	if( $image ) {
		echo wp_get_attachment_image( $image, $size );
	}

	?>
	<?php
	wp_login_form(
		array(
			'remember'       => false,
			'label_username' => __( 'Your login', 'circulab' ),
			'label_password' => __( 'Your password', 'circulab' ),
			'redirect'       => get_permalink(get_field("redirect_page", "options")),
		)
	);
	?>
	<a href="<?php echo wp_lostpassword_url( home_url() ); ?>" class="button buttonGhost"><?php _e("Forgotten password", "circulab");?></a>
</div>
<?php endif;?>

<div id="page">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Go to the content', 'circulab' ); ?></a>
	<a class="skip-link screen-reader-text" href="#menu"><?php esc_html_e( 'Back to the menu', 'circulab' ); ?></a>

	<header id="masthead">

		<ul class="menu-website">
			
			<li id="menu-website-open">
				<?php
					//logo champs ACF la taille "logo" est gérée depuis le fichier inc/image.php
					$image = get_field('courent_flag','option');
					$size = 'flag';
					if( $image ) {
						echo wp_get_attachment_image( $image, $size );
					}
					the_field('libelle_site_courant','option');
				?>
				<ul>
					<?php
					if( have_rows('autres_sites', 'option') ):
						while( have_rows('autres_sites', 'option') ) : the_row();
							$image = get_sub_field('flag');
							$size = 'flag';
							
						?>
							<li>
								<a href="<?php the_sub_field('url');?>?redirectLang">
									<?php
										if( $image ) {
											echo wp_get_attachment_image( $image, $size );
										}
									?>
									<?php the_sub_field('libelle');?>
								</a>
							</li>
						<?php	
						endwhile;
					endif;
					?>
				</ul>
				</li>
			</ul>

		<div class="wrapper">

			<!-- Container of the topbar menu -->
			<div class="menuTopbar">

				<!-- Custom logo for desktop only  -->
				<div class="menuTopbarLogoDesktop" id="menuTopbarLogo">
					<!-- Custom Logo -> link to homepage -->
					<a class="custom_logo" href="<?php echo home_url();?>">
						<?php
						//logo champs ACF la taille "logo" est gérée depuis le fichier inc/image.php
						$image = get_field('custom_logo','option');
						$size = 'logo';
						if( $image ) {
							echo wp_get_attachment_image( $image, $size );
						}
						?>
					</a>

					<!-- Button -> Open menu-mobile -->
					<button class="OpenMobile burgerButton isHiddenOnDesktop" onclick="toggleMenuMobile()">
						<svg xmlns="http://www.w3.org/2000/svg" width="35.284" height="23.522" viewBox="0 0 35.284 23.522">
					  	<path id="burger" d="M0,3.92V0H35.284V3.92Zm0,9.8H35.284V9.8H0Zm0,9.8H35.284V19.6H0Z" transform="translate(0 0)" fill-rule="evenodd"/>
						</svg>
						<p>Menu</p>
					</button>

				</div>

				<!-- Container of the public menu + lang select + connection menu -->
				<nav class="menuTopbarNav" id="menuMobile">

					<button class="closeMobile rounded-corner isHiddenOnDesktop" onclick="toggleMenuMobile()">X</button>

					<!-- Public menu -->
					<div id="menu"><!-- menu principal -->
						<?php
							echo ihag_menu('primary');
						?>
					</div>


					<!-- Lang select - Polylang plugin -->
					<!--<div class="polylang_switch">
						<?php // Select de la langue - Polylang - list of languages names // source @https://polylang.wordpress.com/documentation/documentation-for-developers/functions-reference/ ?>
						<?php // pll_the_languages(array('dropdown'=>1, 'show_flags'=>1,'show_names'=>0));  
							weglot_get_button_selector_html();
						?>
					</div>-->


					<!-- Connection Menu -->
					<?php if(get_current_blog_id() == 1):?>
					<div class="menuConnectContainer">
						<?php if(is_user_logged_in()): //si l'utilisateur est conecté?>

							<div class="separator-thin hasWhiteBg isHiddenOnDesktop"></div>

							<?php
								$user_id = get_current_user_id();
								if(get_field("image", "user_".$user_id)){
									$image = get_field("image", "user_".$user_id);
									$size = 'thumbnail';
									if( $image ) {
										echo '<div class="avatarContainer">';
											echo wp_get_attachment_image( $image, $size );
										echo '</div>';
									}
								}
								else{
									echo '<div class="avatarContainer">';
									echo get_avatar( get_the_author_meta( 'ID' ), 32 );
									echo '</div>';
								}?>


							<?php
								echo '<button class="toggleConnect isHiddenOnMobile" onclick="toggleMenuConnect()">';
									echo '<svg xmlns="http://www.w3.org/2000/svg" width="35.284" height="23.522" viewBox="0 0 35.284 23.522">';
										echo '<path id="burger" d="M0,3.92V0H35.284V3.92Zm0,9.8H35.284V9.8H0Zm0,9.8H35.284V19.6H0Z" transform="translate(0 0)" fill-rule="evenodd"/>';
									echo '</svg>';
								echo '</button>';
								echo '<nav id="menuConnect">';
									echo ihag_menu('connect');//menu connecté
								echo '</nav>';

								echo '<div class="separator-thin hasWhiteBg isHiddenOnDesktop"></div>';
							?>

						<?php else:?>
							<button id="connectUser" class="buttonModal buttonHeader" data-id-modal="connectModal"><?php _e( "Sign in", "circulab" );?></button>
						<?php endif;?>

					</div>
					<?php endif;?>
				</nav>
			</div>
		</div>
	</header>

	<div id="content">

		<div class="wrapper pageTitleContainer"><!-- Page Title -->

			<?php
			if (is_front_page()):?>
				<h1 class="watermark homeTitle"><?php the_title();?></h1>
				<span class="pageExcerpt"><?php echo $post->post_excerpt;?></span>
			<?php elseif(is_singular()):?>
				<h1 class="pageTitle"><?php the_excerpt();?></h1>
			<?php elseif(is_archive()):?>
				<h1 class="pageTitle"><?php echo get_the_archive_description();?></h1>
			<?php endif;?>

		</div>




