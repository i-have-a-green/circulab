<?php

include_once("inc/widget.php");
include_once("inc/clean.php");
include_once("inc/no-comment.php");
include_once("inc/images.php");
include_once("inc/custom-post-type.php");
include_once("inc/breadcrumb.php");
include_once("inc/menu.php");
include_once("inc/acf.php");
include_once("inc/acf-block.php");
include_once("inc/enqueue_scripts.php");
//include_once("inc/contact.php");
include_once("inc/event.php");
include_once("inc/user.php");

// Adding excerpt for page
add_action( 'init', 'my_custom_init' );
function my_custom_init() {
	add_post_type_support( 'page', 'excerpt' );
	// If not in the admin, return.
    if ( ! is_admin() ) {
		return;
	 }
  
	 // Get the post ID on edit post with filter_input super global inspection.
	 $current_post_id = filter_input( INPUT_GET, 'post', FILTER_SANITIZE_NUMBER_INT );
	 // Get the post ID on update post with filter_input super global inspection.
	 $update_post_id = filter_input( INPUT_POST, 'post_ID', FILTER_SANITIZE_NUMBER_INT );
  
	 // Check to see if the post ID is set, else return.
	 if ( isset( $current_post_id ) ) {
		$post_id = absint( $current_post_id );
	 } else if ( isset( $update_post_id ) ) {
		$post_id = absint( $update_post_id );
	 } else {
		return;
	 }
  
	 // Don't do anything unless there is a post_id.
	 if ( isset( $post_id ) ) {
		// Get the template of the current post.
		$template_file = get_post_meta( $post_id, '_wp_page_template', true );
  
		// Example of removing page editor for page-your-template.php template.
		if (  'template/page-community.php' !== $template_file ) {
			remove_post_type_support( 'page', 'thumbnail' );
			// Other features can also be removed in addition to the editor. See: https://codex.wordpress.org/Function_Reference/remove_post_type_support.
		}
	 }

}

/**
 * Filter the excerpt length to 200 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
add_filter( 'excerpt_length', function( $length ) { return 200; } );

function ihag_unregister_taxonomy(){
    unregister_taxonomy_for_object_type( 'post_tag', 'post' );
	unregister_taxonomy_for_object_type( 'category', 'post' );
}
add_action('init', 'ihag_unregister_taxonomy'); 

// Copier le contenu d'une page ou d'un post à la création d'une traduction
function cw2b_content_copy( $content ) {    
    if ( isset( $_GET['from_post'] ) ) {
        $my_post = get_post( $_GET['from_post'] );
        if ( $my_post )
            return $my_post->post_content;
    }
    return $content;
}
//add_filter( 'default_content', 'cw2b_content_copy' );

add_action( 'init', 'nopio_register_user_category_taxonomy' );

function nopio_register_user_category_taxonomy() {
	register_taxonomy(
		'USER_CATEGORY_NAME', //taxonomy name
		'user', //object for which the taxonomy is created
		array( //taxonomy details
			'public' => true,
			'labels' => array(
				'name'		=> 'User Categories',
				'singular_name'	=> 'User Category',
				'menu_name'	=> 'User Categories',
				'search_items'	=> 'Search User Category',
				'popular_items' => 'Popular User Categories',
				'all_items'	=> 'All User Categories',
				'edit_item'	=> 'Edit User Category',
				'update_item'	=> 'Update User Category',
				'add_new_item'	=> 'Add New User Category',
				'new_item_name'	=> 'New User Category Name',
			),
			'update_count_callback' => function() {
				return; //important
			}
		)
	);
}


function wpgreen_display_users(){

	$display_users = json_decode( stripslashes( $_POST['display_users'] ) );
	switch_to_blog(1);
	foreach ($display_users as $userId) {
		
		set_query_var( 'user_id', absint( $userId ) );
    	get_template_part('template-parts/archive', "community");
	}
	restore_current_blog();
	//wp_die();
}

add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'display_users',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'wpgreen_display_users',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);
});